# Bike Share #

## Product Flavors

This Android project has 2 product flavors:

* **mock** - app is driven by hardcoded data (JSON string), downloaded from the Melbourne bike share data service
* **prod** - app is driven by real data coming from the [Melbourne bike share service](https://data.melbourne.vic.gov.au/Transport-Movement/Melbourne-bike-share/tdvh-n9dv/)

## Testing

Tests are in the following directories: 

* **androidTest** - integration (BDD) tests backed by a mocked web server
* **test** - junit tests

Tests can be run by creating the Run/Debug configurations over the above directories from Android Studio.

## Running the App

The app can be run or debugged from Android Studio with either the **mockDebug** or **prodDebug** build variants.
