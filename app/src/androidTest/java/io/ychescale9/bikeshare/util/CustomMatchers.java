package io.ychescale9.bikeshare.util;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import io.ychescale9.bikeshare.presentation.helper.MarkerInfo;

import static android.support.test.espresso.intent.Checks.checkArgument;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.closeTo;

/**
 * Created by yang on 22/10/16.
 */
public final class CustomMatchers {

    /**
     * Returns a matcher that matches Pair<Marker, MarkerInfo> that corresponds to
     * the values in the arguments
     *
     * @param stationId
     * @param featureName
     * @param coordinates
     * @param markerSize
     * @param markerColor
     * @return
     */
    public static Matcher<Pair<Marker, MarkerInfo>> isSameMarker(
            final Long stationId,
            final String featureName,
            final LatLng coordinates,
            final double latLngErrorAllowed,
            final int markerSize,
            final int markerColor) {

        return new TypeSafeMatcher<Pair<Marker, MarkerInfo>>() {
            @Override
            public boolean matchesSafely(Pair<Marker, MarkerInfo> markerWithInfo) {
                Marker marker = markerWithInfo.first;
                MarkerInfo markerInfo = markerWithInfo.second;
                return TextUtils.equals(marker.getTitle(), featureName) &&
                        isSameLocation(marker.getPosition(), latLngErrorAllowed).matches(coordinates) &&
                        markerInfo.stationId.longValue() == stationId.longValue() &&
                        TextUtils.equals(markerInfo.title, featureName) &&
                        isSameLocation(markerInfo.latLng, latLngErrorAllowed).matches(coordinates) &&
                        markerInfo.size == markerSize &&
                        markerInfo.color == markerColor;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is the same marker");
            }
        };
    }

    /**
     * Returns a matcher that matches the location in the argument with allowing some error range
     * @param location
     * @param errorAllowed
     * @return
     */
    public static Matcher<LatLng> isSameLocation(final LatLng location, final double errorAllowed) {
        return new TypeSafeMatcher<LatLng>() {
            @Override
            protected boolean matchesSafely(LatLng targetLocation) {
                return closeTo(targetLocation.latitude, errorAllowed).matches(location.latitude) &&
                        closeTo(targetLocation.longitude, errorAllowed).matches(location.longitude);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is (approximately) the same location");
            }
        };
    }

    /**
     * Returns a matcher that matches the item within a RecyclerView
     * @param itemText
     * @return
     */
    public static Matcher<View> withItemText(final String itemText) {
        checkArgument(!TextUtils.isEmpty(itemText),"cannot be null");
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                return allOf(isDescendantOfA(isAssignableFrom(RecyclerView.class)),withText(itemText)).matches(item);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is descendant of a RecyclerView with text" + itemText);
            }
        };
    }
}
