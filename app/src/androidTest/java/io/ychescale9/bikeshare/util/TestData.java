package io.ychescale9.bikeshare.util;

/**
 * Created by yang on 21/10/16.
 */
public class TestData {

    public static final String BIKE_STATIONS_JSON =
            "[{\"coordinates\":{\"type\":\"Point\",\"coordinates\":[144.966542,-37.826578]},\"featurename\":\"ACCA - Sturt St - Southbank\",\"id\":\"50\",\"nbbikes\":\"18\",\"nbemptydoc\":\"5\",\"terminalname\":\"60043\",\"uploaddate\":\"2016-10-22T23:30:10.000\"}\n" +
            ",{\"coordinates\":{\"type\":\"Point\",\"coordinates\":[144.944488,-37.821568]},\"featurename\":\"ANZ - Collins St - Docklands\",\"id\":\"51\",\"nbbikes\":\"8\",\"nbemptydoc\":\"11\",\"terminalname\":\"60044\",\"uploaddate\":\"2016-10-22T23:30:10.000\"}\n" +
            ",{\"coordinates\":{\"type\":\"Point\",\"coordinates\":[144.955223,-37.809216]},\"featurename\":\"Flagstaff Gardens - Peel St - West Melbourne\",\"id\":\"52\",\"nbbikes\":\"4\",\"nbemptydoc\":\"7\",\"terminalname\":\"60048\",\"uploaddate\":\"2016-10-22T23:30:10.000\"}\n" +
            ",{\"coordinates\":{\"type\":\"Point\",\"coordinates\":[144.959017,-37.806091]},\"featurename\":\"Victoria Market - Elizabeth St / Victoria St - City\",\"id\":\"53\",\"nbbikes\":\"17\",\"nbemptydoc\":\"8\",\"terminalname\":\"60049\",\"uploaddate\":\"2016-10-22T23:30:10.000\"}\n" +
            ",{\"coordinates\":{\"type\":\"Point\",\"coordinates\":[144.982006,-37.816731]},\"featurename\":\"Jolimont Station - Wellington Pde South - East Melbourne\",\"id\":\"49\",\"nbbikes\":\"5\",\"nbemptydoc\":\"6\",\"terminalname\":\"60042\",\"uploaddate\":\"2016-10-22T23:30:10.000\"}]\n";

}
