package io.ychescale9.bikeshare.util;

/**
 * Created by yang on 22/10/16.
 */

public class TestUtil {

    /**
     * The error delta allowed when verifying latitude or longitude
     */
    public final static double LAT_LNG_ERROR_ALLOWED = 0.01;

    /**
     * The port used for the mock server
     */
    public final static int MOCK_SERVER_PORT = 50000;
}
