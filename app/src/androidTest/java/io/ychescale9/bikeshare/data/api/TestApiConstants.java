package io.ychescale9.bikeshare.data.api;

import io.ychescale9.bikeshare.util.TestUtil;

/**
 * Created by yang on 25/10/16.
 */
public class TestApiConstants extends ApiConstants {
    @Override
    public String getApiEndpoint() {
        return "http://127.0.0.1:" + TestUtil.MOCK_SERVER_PORT + "/";
    }
}
