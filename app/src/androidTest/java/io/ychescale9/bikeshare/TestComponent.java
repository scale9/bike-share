package io.ychescale9.bikeshare;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import io.ychescale9.bikeshare.data.api.TestApiModule;
import io.ychescale9.bikeshare.data.cache.CacheModule;
import io.ychescale9.bikeshare.data.repository.RepositoryModule;
import io.ychescale9.bikeshare.presentation.base.ActivityModule;
import io.ychescale9.bikeshare.ui.BaseUiTest;

/**
 * Created by yang on 22/10/16.
 */
@Singleton
@Component(
        modules = {
                TestAppModule.class,
                TestConstantsModule.class,
                RepositoryModule.class,
                TestApiModule.class,
                CacheModule.class,
                ActivityModule.class, // all activity sub-components
        }
)
public interface TestComponent extends AppComponent {

    void inject(BaseUiTest test);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(Application application);

        TestComponent build();
    }
}
