package io.ychescale9.bikeshare.ui;

import android.app.Instrumentation;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.inject.Inject;

import io.ychescale9.bikeshare.TestBikeShareApplication;
import io.ychescale9.bikeshare.TestComponent;
import io.ychescale9.bikeshare.data.cache.BikeStationsDataCache;
import io.ychescale9.bikeshare.data.util.BikeStationJsonDeserializer;
import io.ychescale9.bikeshare.presentation.UiConstants;
import io.ychescale9.bikeshare.util.SystemAnimationsUtil;
import io.ychescale9.bikeshare.util.TestUtil;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * Created by yang on 22/10/16.
 */
public abstract class BaseUiTest {

    @Inject
    UiConstants uiConstants;

    @Inject
    BikeStationJsonDeserializer bikeStationJsonDeserializer;

    @Inject
    BikeStationsDataCache bikeStationsDataCache;

    MockWebServer mockWebServer;

    TestBikeShareApplication testApp;

    @BeforeClass
    public static void setUpBeforeClass() throws Throwable {
        // disable animations before running tests
        SystemAnimationsUtil.disableAnimations(getInstrumentation().getTargetContext());
    }

    @AfterClass
    public static void tearDownAfterClass() throws Throwable {
        // re-enabled animations after running tests
        SystemAnimationsUtil.enableAnimations(getInstrumentation().getTargetContext());
    }

    @Before
    public void setUp() throws Throwable {
        Instrumentation instrumentation = getInstrumentation();
        testApp = (TestBikeShareApplication) instrumentation.getTargetContext().getApplicationContext();
        TestComponent component = (TestComponent) testApp.getComponent();
        component.inject(this);

        // start a new mock server
        mockWebServer = new MockWebServer();
        mockWebServer.start(TestUtil.MOCK_SERVER_PORT);
    }

    @After
    public void tearDown() throws Throwable {
        // clear cache data after each test
        bikeStationsDataCache.clear();

        // shut down mock server
        mockWebServer.shutdown();
    }
}
