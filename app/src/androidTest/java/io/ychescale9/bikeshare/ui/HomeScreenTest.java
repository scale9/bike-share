package io.ychescale9.bikeshare.ui;

import android.app.Instrumentation;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.ContextCompat;
import android.util.LongSparseArray;
import android.util.Pair;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.ychescale9.bikeshare.R;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.data.util.ModelUtil;
import io.ychescale9.bikeshare.presentation.helper.MarkerInfo;
import io.ychescale9.bikeshare.presentation.home.HomeActivity;
import io.ychescale9.bikeshare.presentation.util.MarkerSizeCalculator;
import io.ychescale9.bikeshare.util.CustomMatchers;
import io.ychescale9.bikeshare.util.TestData;
import io.ychescale9.bikeshare.util.TestUtil;
import io.ychescale9.bikeshare.util.ViewAtLeastPartiallyOnScreenIdlingResource;
import okhttp3.mockwebserver.MockResponse;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static io.ychescale9.bikeshare.util.CustomMatchers.isSameMarker;
import static io.ychescale9.bikeshare.util.CustomMatchers.withItemText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assume.assumeThat;

/**
 * Created by yang on 21/10/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class HomeScreenTest extends BaseUiTest {

    @Rule
    public IntentsTestRule<HomeActivity> activityRule = new IntentsTestRule<>(
            HomeActivity.class,
            true,
            false); // launch activity manually

    private final List<BikeStation> dummyBikeStations = new ArrayList<>();

    @Override
    public void setUp() throws Throwable {
        super.setUp();

        // only execute tests if Google API (Google Play Services) is available on the device
        assumeThat(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(testApp.getApplicationContext()), is(ConnectionResult.SUCCESS));

        // create Gson object with custom deserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(BikeStation.class, bikeStationJsonDeserializer);
        Gson gson = gsonBuilder.create();
        // convert the dummy bike stations from json to List<BikeStation>
        Type listType = new TypeToken<List<BikeStation>>() {}.getType();
        List<BikeStation> bikeStations = gson.fromJson(TestData.BIKE_STATIONS_JSON, listType);
        dummyBikeStations.clear();
        dummyBikeStations.addAll(bikeStations);
    }

    @Override
    public void tearDown() throws Throwable {
        super.tearDown();
    }

    @Test
    public void openHomeScreen_mapInitializedAndMarkersRendered() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        activityRule.launchActivity(null);

        // Verify that the map is displayed
        onView(withId(R.id.map_container)).check(matches(isDisplayed()));

        // Verify that the initial map camera position is set to Melbourne
        mapCameraIsSetToMelbourne();

        // Verify that the search edit text is enabled
        searchBikeStationsEditTextIsEnabled();

        // Verify that the correct markers with info are rendered
        markersAreRendered();
    }

    @Test
    public void openHomeScreenWithNoNetwork_showErrorMessage() throws Throwable {
        // given mock server returns error initially
        appendErrorResponse();

        activityRule.launchActivity(null);

        // Verify that the search edit text is disabled
        searchBikeStationsEditTextIsDisabled();

        // Verify that the load bike stations data error is displayed
        loadBikeStationsDataErrorIsDisplayed();
    }

    @Test
    public void retryLoadingBikeStationsData_stationMarkersRendered() throws Throwable {
        // given mock server returns error initially
        appendErrorResponse();
        // given mock server then returns valid response
        appendValidResponse();

        activityRule.launchActivity(null);

        // Idling resource to make sure the snackbar action button is at least partially on the screen
        // before performing click action on it
        IdlingResource idlingResource = new ViewAtLeastPartiallyOnScreenIdlingResource(
                activityRule.getActivity().findViewById(android.support.design.R.id.snackbar_action)
        );

        Espresso.registerIdlingResources(idlingResource);

        // click on the retry button on the snackbar
        onView(withText(R.string.button_text_retry)).perform(click());

        Espresso.unregisterIdlingResources(idlingResource);

        // Verify that the search edit text is enabled after successfully loading data after retrying
        searchBikeStationsEditTextIsEnabled();

        // Verify that the correct markers with info are rendered
        markersAreRendered();
    }

    @Test
    public void clickSearchBox_searchModeStarted() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        activityRule.launchActivity(null);

        // click on the search box
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click());

        // Verify that the search icon is NOT displayed
        searchIconIsNotDisplayed();

        // Verify that the cancel button is displayed
        cancelSearchButtonIsDisplayed();

        // Verify that the clear button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the search box has focus
        searchBoxHasFocus();
    }

    @Test
    public void clickSearchBoxWithStationSelected_searchModeStartedAndResultListDisplayed() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given a bike station to be matched and selected
        BikeStation selectedBikeStation = dummyBikeStations.get(0);

        activityRule.launchActivity(null);

        // select the bike station
        selectBikeStation(selectedBikeStation);

        // click on the search box
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click());

        // Verify that the search icon is NOT displayed
        searchIconIsNotDisplayed();

        // Verify that the cancel button is displayed
        cancelSearchButtonIsDisplayed();

        // Verify that the clear button is displayed
        clearButtonIsDisplayed();

        // Verify that the search box has focus
        searchBoxHasFocus();

        // Verify that the search results list is displayed
        searchResultsListIsDisplayed();

        // Verify that the station info view is NOT displayed
        stationInfoViewIsNotDisplayed();
    }

    @Test
    public void enterSearchQueryWithMatches_matchedResultsDisplayed() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given a bike station to be matched
        BikeStation bikeStation = dummyBikeStations.get(0);
        String queryString = bikeStation.featureName.substring(3);

        activityRule.launchActivity(null);

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // Verify that the search results list is displayed
        searchResultsListIsDisplayed();

        // Verify that the correct match is displayed in the results list
        onView(withItemText(bikeStation.featureName))
                .check(matches(isDisplayed()));
    }

    @Test
    public void enterSearchQueryWhileCacheNotAvailable_allBikeStationsDataReloaded() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // append another valid response for the reload
        appendValidResponse();

        // given a bike station to be matched
        BikeStation bikeStation = dummyBikeStations.get(0);
        String queryString = bikeStation.featureName.substring(3);

        activityRule.launchActivity(null);

        // assume the cache has expired
        bikeStationsDataCache.clear();

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // Verify that the correct match is displayed in the results list
        onView(withItemText(bikeStation.featureName))
                .check(matches(isDisplayed()));

        // Verify that the cache has been updated
        assertThat(bikeStationsDataCache.isExpired(), is(false));
    }

    @Test
    public void enterSearchQueryWithNoMatches_noMatchMessageDisplayed() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given query string with no matches
        String queryString = "bla";

        activityRule.launchActivity(null);

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();

        // Verify that the no search results prompt is displayed
        noSearchResultsPromptIsDisplayed();
    }

    @Test
    public void clearSearchQuery_searchBoxEmpty() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given an arbitrary query string
        String queryString = "melbourne";

        activityRule.launchActivity(null);

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // click on the clear button
        onView(withId(R.id.button_clear))
                .perform(click());

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();

        // Verify that the no search results prompt is NOT displayed
        noSearchResultsPromptIsNotDisplayed();

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the clear button is NOT displayed
        clearButtonIsNotDisplayed();
    }

    @Test
    public void clearSearchQueryByTextReplace_searchBoxEmpty() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given an arbitrary query string
        String queryString = "melbourne";

        activityRule.launchActivity(null);

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // clear the text box by replacing it with ""
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(replaceText(""));

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();

        // Verify that the no search results prompt is NOT displayed
        noSearchResultsPromptIsNotDisplayed();

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the clear button is NOT displayed
        clearButtonIsNotDisplayed();
    }

    @Test
    public void cancelSearch_searchModeEnded() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given an arbitrary query string
        String queryString = "melbourne";

        activityRule.launchActivity(null);

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // click on the cancel button
        onView(withId(R.id.button_cancel_search))
                .perform(click());

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();

        // Verify that the no search results prompt is NOT displayed
        noSearchResultsPromptIsNotDisplayed();

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the clear button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the station info view is NOT displayed
        stationInfoViewIsNotDisplayed();
    }

    @Test
    public void cancelSearchWithStationSelected_searchModeEndedButStationRemainsSelected() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given a bike station to be selected
        BikeStation selectedBikeStation = dummyBikeStations.get(0);

        // given an arbitrary query string
        String queryString = "melbourne";

        activityRule.launchActivity(null);

        // select the bike station
        selectBikeStation(selectedBikeStation);

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // click on the cancel button
        onView(withId(R.id.button_cancel_search))
                .perform(click());

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();

        // Verify that the no search results prompt is NOT displayed
        noSearchResultsPromptIsNotDisplayed();

        // Verify that the search box is showing the selected bike station feature name
        onView(withId(R.id.edit_text_search_bike_stations))
                .check(matches(withText(equalTo(selectedBikeStation.featureName))));

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the clear button is displayed
        clearButtonIsDisplayed();

        // Verify that the station info view is displayed
        stationInfoViewIsDisplayed();
    }

    @Test
    public void cancelSearchByBackPressed_searchModeEnded() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given an arbitrary query string
        String queryString = "melbourne";

        activityRule.launchActivity(null);

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // close softKeyboard first
        closeSoftKeyboard();

        // click on back button
        pressBack();

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();

        // Verify that the no search results prompt is NOT displayed
        noSearchResultsPromptIsNotDisplayed();

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the clear button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the station info view is NOT displayed
        stationInfoViewIsNotDisplayed();
    }

    @Test
    public void cancelSearchByBackPressedWithStationSelected_searchModeEndedButStationRemainsSelected() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given a bike station to be selected
        BikeStation selectedBikeStation = dummyBikeStations.get(0);

        // given an arbitrary query string
        String queryString = "melbourne";

        activityRule.launchActivity(null);

        // select the bike station
        selectBikeStation(selectedBikeStation);

        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // close softKeyboard first
        closeSoftKeyboard();

        // click on back button
        pressBack();

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();

        // Verify that the no search results prompt is NOT displayed
        noSearchResultsPromptIsNotDisplayed();

        // Verify that the search box is showing the selected bike station feature name
        onView(withId(R.id.edit_text_search_bike_stations))
                .check(matches(withText(equalTo(selectedBikeStation.featureName))));

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the clear button is displayed
        clearButtonIsDisplayed();

        // Verify that the station info view is displayed
        stationInfoViewIsDisplayed();
    }

    @Test
    public void clickSearchResult_stationInfoViewDisplayed() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given a bike station to be selected
        BikeStation selectedBikeStation = dummyBikeStations.get(0);

        activityRule.launchActivity(null);

        // select the bike station
        selectBikeStation(selectedBikeStation);

        // Verify that the search results list is NOT displayed
        searchResultsListIsNotDisplayed();

        // Verify that the no search results prompt is NOT displayed
        noSearchResultsPromptIsNotDisplayed();

        // Verify that the search box is showing the selected bike station feature name
        onView(withId(R.id.edit_text_search_bike_stations))
                .check(matches(withText(equalTo(selectedBikeStation.featureName))));

        // Verify that the search icon is displayed
        searchIconIsDisplayed();

        // Verify that the cancel button is NOT displayed
        cancelSearchButtonIsNotDisplayed();

        // Verify that the clear button is displayed
        clearButtonIsDisplayed();

        // Verify that the station info view is displayed
        stationInfoViewIsDisplayed();
    }

    @Test
    public void clearStationSelection_stationInfoViewDismissed() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given a bike station to be selected
        BikeStation selectedBikeStation = dummyBikeStations.get(0);

        activityRule.launchActivity(null);

        // select the bike station
        selectBikeStation(selectedBikeStation);

        // click on the clear button to clear the selection
        onView(withId(R.id.button_clear)).perform(click());

        // Verify that the search box is empty
        searchBoxIsEmpty();

        // Verify that the clear button is NOT displayed
        clearButtonIsNotDisplayed();

        // Verify that the station info view is NOT displayed
        stationInfoViewIsNotDisplayed();
    }

    @Test
    public void clickGetDirectionsButton_googleMapIntentLaunched() throws Throwable {
        // given mock server returns valid response
        appendValidResponse();

        // given a bike station to be matched and selected
        BikeStation selectedBikeStation = dummyBikeStations.get(0);
        String uriStringFormat = "https://maps.google.com/maps?f=d&daddr=%s,%s";
        String uriString = String.format(
                uriStringFormat,
                selectedBikeStation.coordinates.latitude,
                selectedBikeStation.coordinates.longitude
        );
        Uri expectedUri = Uri.parse(uriString);

        activityRule.launchActivity(null);

        // select the bike station
        selectBikeStation(selectedBikeStation);

        // get the expected intent
        Matcher<Intent> expectedIntent = allOf(
                hasAction(equalTo(Intent.ACTION_VIEW)),
                hasData(expectedUri),
                toPackage("com.google.android.apps.maps")
        );

        // intercept the intent
        intending(expectedIntent).respondWith(new Instrumentation.ActivityResult(0, null));

        // click on the get directions button
        onView(withId(R.id.fab_get_directions))
                .perform(click());

        // Verify that the correct intent has been launched
        intended(expectedIntent);
    }

    //================================================================================
    // ACTIONS
    //================================================================================

    private void selectBikeStation(@NonNull BikeStation bikeStation) {
        String queryString = bikeStation.featureName;
        // type in search query
        onView(withId(R.id.edit_text_search_bike_stations))
                .perform(click())
                .perform((typeText(queryString)));

        // select the search result
        onView(withId(R.id.recycler_view_search_results))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    }


    //================================================================================
    // ASSERTIONS
    //================================================================================

    private void mapCameraIsSetToMelbourne() throws Throwable {
        final HomeActivity activity = activityRule.getActivity();
        activityRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LatLng mapCameraPosition = activity.getMapCameraPosition();
                LatLng melbourneLatLng = uiConstants.getMelbourneLatLng();
                double latLngErrorAllowed = TestUtil.LAT_LNG_ERROR_ALLOWED;
                assertThat(mapCameraPosition, CustomMatchers.isSameLocation(melbourneLatLng, latLngErrorAllowed));
            }
        });
    }

    private void markersAreRendered() throws Throwable {
        final HomeActivity activity = activityRule.getActivity();
        activityRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                LongSparseArray<Pair<Marker, MarkerInfo>> markersWithInfo = activity.getMarkersWithInfo();
                List<BikeStation> bikeStations = dummyBikeStations;
                assertThat(markersWithInfo.size(), is(bikeStations.size()));
                for (BikeStation bikeStation : bikeStations) {
                    Pair<Marker, MarkerInfo> markerWithInfo = markersWithInfo.get(bikeStation.id);
                    try {
                        assertThat(markerWithInfo, isSameMarker(
                                bikeStation.id,
                                bikeStation.featureName,
                                bikeStation.coordinates,
                                TestUtil.LAT_LNG_ERROR_ALLOWED,
                                MarkerSizeCalculator.calculateMarkerSizeInPixel(
                                        getInstrumentation().getTargetContext().getResources()
                                                .getDimensionPixelSize(R.dimen.min_marker_size),
                                        getInstrumentation().getTargetContext().getResources()
                                                .getDimensionPixelSize(R.dimen.max_marker_size),
                                        bikeStation.bikesAvailable,
                                        ModelUtil.getMaxBikesAvailable(bikeStations)),
                                getMarkerColor())
                        );
                    } catch (Exception ignored) {}
                }
            }
        });
    }

    private void searchBikeStationsEditTextIsEnabled() {
        onView(withId(R.id.edit_text_search_bike_stations)).check(matches(isEnabled()));
    }

    private void searchBikeStationsEditTextIsDisabled() {
        onView(withId(R.id.edit_text_search_bike_stations)).check(matches(not(isEnabled())));
    }

    private void loadBikeStationsDataErrorIsDisplayed() {
        onView(withText(R.string.error_message_could_not_load_bike_stations))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));

        onView(withText(R.string.button_text_retry))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void searchIconIsDisplayed() {
        onView(withId(R.id.image_view_search))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void searchIconIsNotDisplayed() {
        onView(withId(R.id.image_view_search))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void cancelSearchButtonIsDisplayed() {
        onView(withId(R.id.button_cancel_search))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void cancelSearchButtonIsNotDisplayed() {
        onView(withId(R.id.button_cancel_search))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void clearButtonIsDisplayed() {
        onView(withId(R.id.button_clear))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void clearButtonIsNotDisplayed() {
        onView(withId(R.id.button_clear))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void searchResultsListIsDisplayed() {
        onView(withId(R.id.card_view_search_results))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void searchResultsListIsNotDisplayed() {
        onView(withId(R.id.card_view_search_results))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void noSearchResultsPromptIsDisplayed() {
        onView(withId(R.id.card_view_no_results_prompt))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void noSearchResultsPromptIsNotDisplayed() {
        onView(withId(R.id.card_view_no_results_prompt))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    private void searchBoxHasFocus() {
        onView(withId(R.id.edit_text_search_bike_stations))
                .check(matches(hasFocus()));
    }

    private void searchBoxHasNoFocus() {
        onView(withId(R.id.edit_text_search_bike_stations))
                .check(matches(not(hasFocus())));
    }

    private void searchBoxIsEmpty() {
        onView(withId(R.id.edit_text_search_bike_stations))
                .check(matches(withText(isEmptyString())));
    }

    private void searchBoxIsNotEmpty() {
        onView(withId(R.id.edit_text_search_bike_stations))
                .check(matches(withText(not(isEmptyString()))));
    }

    private void stationInfoViewIsDisplayed() {
        onView(withId(R.id.station_info_view))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(withId(R.id.fab_get_directions))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(withId(R.id.shadow))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    private void stationInfoViewIsNotDisplayed() {
        onView(withId(R.id.station_info_view))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.fab_get_directions))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.shadow))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    //================================================================================
    // HELPERS
    //================================================================================

    private int getMarkerColor() {
        return ContextCompat.getColor(getInstrumentation().getTargetContext(), R.color.marker);
    }

    private void appendValidResponse() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(TestData.BIKE_STATIONS_JSON)
        );
    }

    private void appendErrorResponse() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(404)
        );
    }
}