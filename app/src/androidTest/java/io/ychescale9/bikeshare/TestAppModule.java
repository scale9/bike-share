package io.ychescale9.bikeshare;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.bikeshare.data.util.BikeStationJsonDeserializer;
import io.ychescale9.bikeshare.util.Clock;
import io.ychescale9.bikeshare.util.RealClock;
import io.ychescale9.bikeshare.util.SchedulerProvider;
import io.ychescale9.bikeshare.util.UiTestSchedulerProvider;

/**
 * Created by yang on 29/4/17.
 */
@Module
public class TestAppModule {

    @Provides
    @Singleton
    Clock provideClock() {
        return new RealClock();
    }

    @Provides
    @Singleton
    BikeStationJsonDeserializer provideBikeStationJsonDeserializer() {
        return new BikeStationJsonDeserializer();
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new UiTestSchedulerProvider();
    }
}
