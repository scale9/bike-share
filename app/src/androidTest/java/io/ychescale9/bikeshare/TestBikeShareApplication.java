package io.ychescale9.bikeshare;

/**
 * Created by yang on 22/10/16.
 */
public class TestBikeShareApplication extends BikeShareApplication {

    /**
     * Create top-level Dagger test component.
     */
    @Override
    protected AppComponent createComponent() {
        return DaggerTestComponent.builder()
                .app(this)
                .build();
    }
}
