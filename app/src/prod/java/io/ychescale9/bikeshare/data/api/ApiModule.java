package io.ychescale9.bikeshare.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.data.util.BikeStationJsonDeserializer;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yang on 20/10/16.
 */
@Module
public class ApiModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(ApiConstants apiConstants) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(apiConstants.getConnectTimeOutSeconds(), TimeUnit.SECONDS)
                .readTimeout(apiConstants.getReadTimeOutSeconds(), TimeUnit.SECONDS)
                .writeTimeout(apiConstants.getWriteTimeOutSeconds(), TimeUnit.SECONDS);
        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client, ApiConstants apiConstants, BikeStationJsonDeserializer deserializer) {
        // create Gson object with custom deserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(BikeStation.class, deserializer);
        Gson gson = gsonBuilder.create();

        return new Retrofit.Builder()
                .baseUrl(apiConstants.getApiEndpoint())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    BikeShareService provideBikeShareService(Retrofit retrofit) {
        return retrofit.create(BikeShareService.class);
    }
}
