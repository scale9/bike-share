package io.ychescale9.bikeshare;

import timber.log.Timber;

/**
 * Created by yang on 9/11/16.
 */
public class BikeShareApplication extends BaseBikeShareApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        // install the debug tree
        Timber.plant(new Timber.DebugTree() {
            @Override
            protected String createStackElementTag(StackTraceElement element) {
                // Include line number in the tag.
                return super.createStackElementTag(element) + ":" + element.getLineNumber();
            }
        });
    }
}
