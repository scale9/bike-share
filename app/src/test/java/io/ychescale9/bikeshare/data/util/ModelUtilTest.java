package io.ychescale9.bikeshare.data.util;

import com.google.android.gms.maps.model.LatLng;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import io.ychescale9.bikeshare.data.model.BikeStation;

import static io.ychescale9.bikeshare.data.util.ModelUtil.ERROR_MESSAGE_BIKE_STATIONS_IS_EMPTY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by yang on 22/10/16.
 * Unit tests for the implementation of {@link ModelUtil}
 */
public class ModelUtilTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {

    }

    @Test
    public void getMaxBikesAvailable_noneAvailableInAllStations() throws Exception {
        List<BikeStation> bikeStations = new ArrayList<>();
        bikeStations.add(new BikeStation(1L, "station 1", "30000", 0, 30, new LatLng(0, 0)));
        bikeStations.add(new BikeStation(2L, "station 2", "30001", 0, 50, new LatLng(0, 0)));
        bikeStations.add(new BikeStation(3L, "station 3", "30002", 0, 100, new LatLng(0, 0)));

        assertThat(ModelUtil.getMaxBikesAvailable(bikeStations), is(0));
    }

    @Test
    public void getMaxBikesAvailable_sameNumberAvailableInAllStations() throws Exception {
        List<BikeStation> bikeStations = new ArrayList<>();
        bikeStations.add(new BikeStation(1L, "station 1", "30000", 20, 30, new LatLng(0, 0)));
        bikeStations.add(new BikeStation(2L, "station 2", "30001", 20, 50, new LatLng(0, 0)));
        bikeStations.add(new BikeStation(3L, "station 3", "30002", 20, 100, new LatLng(0, 0)));

        assertThat(ModelUtil.getMaxBikesAvailable(bikeStations), is(20));
    }

    @Test
    public void getMaxBikesAvailable_differentNumbersAvailable() throws Exception {
        List<BikeStation> bikeStations = new ArrayList<>();
        bikeStations.add(new BikeStation(1L, "station 1", "30000", 0, 30, new LatLng(0, 0)));
        bikeStations.add(new BikeStation(2L, "station 2", "30001", 40, 50, new LatLng(0, 0)));
        bikeStations.add(new BikeStation(3L, "station 3", "30002", 75, 100, new LatLng(0, 0)));
        bikeStations.add(new BikeStation(4L, "station 4", "30003", 5, 10, new LatLng(0, 0)));
        bikeStations.add(new BikeStation(5L, "station 5", "30004", 60, 60, new LatLng(0, 0)));

        assertThat(ModelUtil.getMaxBikesAvailable(bikeStations), is(75));
    }

    @Test
    public void getMaxBikesAvailable_emptyBikeStations() throws Exception {
        thrown.expectMessage(ERROR_MESSAGE_BIKE_STATIONS_IS_EMPTY);
        ModelUtil.getMaxBikesAvailable(new ArrayList<BikeStation>());
    }
}
