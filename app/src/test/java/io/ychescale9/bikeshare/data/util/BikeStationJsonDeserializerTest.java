package io.ychescale9.bikeshare.data.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.junit.Before;
import org.junit.Test;

import io.ychescale9.bikeshare.data.model.BikeStation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;

/**
 * Created by yang on 22/10/16.
 * Unit tests for the implementation of {@link BikeStationJsonDeserializer}
 */
public class BikeStationJsonDeserializerTest {

    final String DUMMY_BIKE_STATION = "{\"coordinates\":{\"type\":\"Point\",\"coordinates\":[144, -37]},\"featurename\":\"King St - CBD\",\"id\":\"3\",\"nbbikes\":\"10\",\"nbemptydoc\":\"5\",\"terminalname\":\"60000\",\"uploaddate\":\"2016-10-22T23:30:10.000\"}";

    private Gson gson;

    @Before
    public void setUp() {
        // create Gson object using BikeStationJsonDeserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(BikeStation.class, new BikeStationJsonDeserializer());
        gson = gsonBuilder.create();
    }

    @Test
    public void deserializeBikeStations_happyCase() throws Exception {
        BikeStation bikeStation = gson.fromJson(DUMMY_BIKE_STATION, BikeStation.class);

        assertNotNull(bikeStation);
        assertThat(bikeStation.id, is(3L));
        assertThat(bikeStation.featureName, is("King St - CBD"));
        assertThat(bikeStation.terminalName, is("60000"));
        assertThat(bikeStation.bikesAvailable, is(10));
        assertThat(bikeStation.emptySlotsAvailable, is(5));
        assertThat(bikeStation.coordinates.latitude, is(-37d));
        assertThat(bikeStation.coordinates.longitude, is(144d));
    }

    @Test(expected = JsonSyntaxException.class)
    public void deserializeBikeStations_invalidJson() throws Exception {
        gson.fromJson("invalid json", BikeStation.class);
    }
}
