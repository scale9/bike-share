package io.ychescale9.bikeshare.data.cache;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.TestObserver;
import io.ychescale9.bikeshare.data.exception.CacheExpiredException;
import io.ychescale9.bikeshare.data.exception.CacheNotAvailableException;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.util.Clock;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 24/10/16.
 * Unit tests for the implementation of {@link BikeStationsDataCacheImpl}
 */
@SuppressWarnings("unchecked")
public class BikeStationsDataCacheImplTest {

    // cache expiration time - 5 minutes
    private final int testExpirationTimeMilliseconds = 5 * 60 * 1000;

    private List<BikeStation> dummyBikeStations;

    @Mock
    private Clock mockClock;

    private TestObserver testObserver;

    private BikeStationsDataCacheImpl bikeStationsDataCacheImpl;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        dummyBikeStations = new ArrayList<>();
        dummyBikeStations.add(new BikeStation(1L, "station 1", "10000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(2L, "station 2", "20000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(3L, "station 3", "30000", 20, 0, null));

        testObserver = new TestObserver();
        bikeStationsDataCacheImpl = new BikeStationsDataCacheImpl(
                testExpirationTimeMilliseconds, mockClock);

        // mock clock returns the real currentTimeMillis by default
        when(mockClock.getCurrentTimeMillis()).thenReturn(System.currentTimeMillis());
    }

    @Test
    public void load_cacheNotAvailable() throws Exception {
        // no data has been written to the cache
        // load cache
        bikeStationsDataCacheImpl.load().subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void load_validCache() throws Exception {
        // put some non-empty data into the cache
        bikeStationsDataCacheImpl.update(dummyBikeStations);
        // load cache
        bikeStationsDataCacheImpl.load().subscribe(testObserver);
        testObserver.assertNoErrors();

        // verify that the correct cache has been loaded
        testObserver.assertValue(dummyBikeStations);
    }

    @Test
    public void load_cacheExpired() throws Exception {
        // put some non-empty data into the cache
        bikeStationsDataCacheImpl.update(dummyBikeStations);
        // advance the clock so the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds + 50);

        // load cache
        bikeStationsDataCacheImpl.load().subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheExpiredException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void update_withEmptyData() throws Exception {
        // put some empty data into the cache
        bikeStationsDataCacheImpl.update(new ArrayList<BikeStation>());
        // load cache
        bikeStationsDataCacheImpl.load().subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void clear() throws Exception {
        // put some non-empty data into the cache
        bikeStationsDataCacheImpl.update(dummyBikeStations);
        // clear the cache
        bikeStationsDataCacheImpl.clear();
        // load cache
        bikeStationsDataCacheImpl.load().subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void isExpired_true() throws Exception {
        // put some non-empty data into the cache
        bikeStationsDataCacheImpl.update(dummyBikeStations);
        // advance the clock so the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds + 50);

        // verify that the cache has expired
        assertThat(bikeStationsDataCacheImpl.isExpired(), is(true));
    }

    @Test
    public void isExpired_false() throws Exception {
        // put some non-empty data into the cache
        bikeStationsDataCacheImpl.update(dummyBikeStations);
        // advance the clock to some time BEFORE the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds - 50);

        // verify that the cache has expired
        assertThat(bikeStationsDataCacheImpl.isExpired(), is(false));
    }
}