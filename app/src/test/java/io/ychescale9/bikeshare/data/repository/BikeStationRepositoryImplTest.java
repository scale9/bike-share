package io.ychescale9.bikeshare.data.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.bikeshare.data.api.BikeShareService;
import io.ychescale9.bikeshare.data.cache.BikeStationsDataCache;
import io.ychescale9.bikeshare.data.exception.CacheExpiredException;
import io.ychescale9.bikeshare.data.exception.CacheNotAvailableException;
import io.ychescale9.bikeshare.data.model.BikeStation;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 23/10/16.
 * Unit tests for the implementation of {@link BikeStationRepositoryImpl}
 */
@SuppressWarnings("unchecked")
public class BikeStationRepositoryImplTest {

    private List<BikeStation> cacheBikeStations;

    private List<BikeStation> remoteBikeStations;

    @Mock
    private BikeStationsDataCache mockBikeStationsDataCache;

    @Mock
    private BikeShareService mockBikeShareService;

    private final String dummyServiceUrl = "http://localhost/test";

    private TestObserver testObserver;

    private BikeStationRepositoryImpl bikeStationRepository;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        cacheBikeStations = new ArrayList<>();
        cacheBikeStations.add(new BikeStation(1L, "station 1", "10000", 20, 0, null));
        cacheBikeStations.add(new BikeStation(2L, "station 2", "20000", 20, 0, null));
        cacheBikeStations.add(new BikeStation(3L, "station 3", "30000", 20, 0, null));

        remoteBikeStations = new ArrayList<>();
        remoteBikeStations.add(new BikeStation(1L, "station 1", "10000", 15, 5, null));
        remoteBikeStations.add(new BikeStation(2L, "station 2", "20000", 10, 10, null));
        remoteBikeStations.add(new BikeStation(3L, "station 3", "30000", 20, 0, null));


        testObserver = new TestObserver();
        bikeStationRepository = new BikeStationRepositoryImpl(
                mockBikeStationsDataCache,
                mockBikeShareService,
                dummyServiceUrl);
    }

    @Test
    public void loadAllBikeStations_noCache() throws Exception {
        // given there's no cache data
        when(mockBikeStationsDataCache.load()).thenReturn(Observable.<List<BikeStation>>error(new CacheNotAvailableException()));
        // given valid remote data
        when(mockBikeShareService.loadAllBikeStations(dummyServiceUrl)).thenReturn(Observable.just(remoteBikeStations));

        bikeStationRepository.loadAllBikeStations().subscribe(testObserver);
        testObserver.assertNoErrors();

        // should update cache with loaded data from server
        verify(mockBikeStationsDataCache).update(ArgumentMatchers.<BikeStation>anyList());

        // verify that the correct data has been loaded from server
        testObserver.assertValue(remoteBikeStations);
    }

    @Test
    public void loadAllBikeStations_hasValidCache() throws Exception {
        // given cache data
        when(mockBikeStationsDataCache.load()).thenReturn(Observable.just(cacheBikeStations));
        // given valid remote data
        when(mockBikeShareService.loadAllBikeStations(dummyServiceUrl)).thenReturn(Observable.just(remoteBikeStations));

        bikeStationRepository.loadAllBikeStations().subscribe(testObserver);
        testObserver.assertNoErrors();

        // should NOT update cache
        verify(mockBikeStationsDataCache, never()).update(remoteBikeStations);

        // verify that the correct data has been loaded from cache
        testObserver.assertValue(cacheBikeStations);
    }

    @Test
    public void loadAllBikeStations_hasExpiredCache() throws Exception {
        // given cache has expired
        when(mockBikeStationsDataCache.load()).thenReturn(Observable.<List<BikeStation>>error(new CacheExpiredException()));
        // given valid remote data
        when(mockBikeShareService.loadAllBikeStations(dummyServiceUrl)).thenReturn(Observable.just(remoteBikeStations));

        bikeStationRepository.loadAllBikeStations().subscribe(testObserver);
        testObserver.assertNoErrors();

        // should try to load from cache
        verify(mockBikeStationsDataCache).load();
        // should then try to load from server
        verify(mockBikeShareService).loadAllBikeStations(dummyServiceUrl);
        // should then update cache with loaded data from server
        verify(mockBikeStationsDataCache).update(ArgumentMatchers.<BikeStation>anyList());

        // verify that the correct data has been loaded from server
        testObserver.assertValue(remoteBikeStations);
    }

    @Test
    public void loadAllBikeStations_noCache_errorLoadingDataFromServer() throws Exception {
        // given there's no cache data
        when(mockBikeStationsDataCache.load()).thenReturn(Observable.<List<BikeStation>>error(new CacheNotAvailableException()));
        // given remote service returns an error
        when(mockBikeShareService.loadAllBikeStations(dummyServiceUrl)).thenReturn(Observable.<List<BikeStation>>error(new Exception("server error")));

        bikeStationRepository.loadAllBikeStations().subscribe(testObserver);
        testObserver.assertError(Exception.class);

        // should try to load from cache
        verify(mockBikeStationsDataCache).load();
        // should then try to load from server
        verify(mockBikeShareService).loadAllBikeStations(dummyServiceUrl);
        // should NOT update cache
        verify(mockBikeStationsDataCache, never()).update(remoteBikeStations);
    }
}