package io.ychescale9.bikeshare.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.bikeshare.TestSchedulerProvider;
import io.ychescale9.bikeshare.data.cache.BikeStationsDataCache;
import io.ychescale9.bikeshare.data.exception.CacheExpiredException;
import io.ychescale9.bikeshare.data.exception.CacheNotAvailableException;
import io.ychescale9.bikeshare.data.model.BikeStation;

import static org.mockito.Mockito.when;

/**
 * Created by yang on 23/10/16.
 * Unit tests for the implementation of {@link SearchBikeStations}
 */
@SuppressWarnings("unchecked")
public class SearchBikeStationsTest {

    private final List<BikeStation> allCachedBikeStations = new ArrayList<>();

    private final BikeStation bikeStation1 = new BikeStation(1L, "station 1", "10000", 20, 0, null);
    private final BikeStation bikeStation2 = new BikeStation(2L, "station 2", "20000", 20, 0, null);
    private final BikeStation bikeStation3 = new BikeStation(3L, "station 3", "30000", 20, 0, null);
    private final BikeStation bikeStation4 = new BikeStation(10L, "station 10", "100000", 20, 0, null);
    private final BikeStation bikeStation5 = new BikeStation(11L, "station 11", "110000", 20, 0, null);
    private final BikeStation bikeStation6 = new BikeStation(12L, "station 12", "120000", 20, 0, null);
    private final BikeStation bikeStation7 = new BikeStation(15L, "Station AB", "150000", 20, 0, null);
    private final BikeStation bikeStation8 = new BikeStation(16L, "Station abc", "160000", 20, 0, null);
    private final BikeStation bikeStation9 = new BikeStation(17L, "Smith st St Kilda", "170000", 20, 0, null);
    private final BikeStation bikeStation10 = new BikeStation(18L, "King st St Kilda", "180000", 20, 0, null);

    @Mock
    private BikeStationsDataCache mockBikeStationsDataCache;

    private TestObserver testObserver;

    private SearchBikeStations searchBikeStations;

    @Before
    public void setUp() {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        allCachedBikeStations.add(bikeStation1);
        allCachedBikeStations.add(bikeStation2);
        allCachedBikeStations.add(bikeStation3);
        allCachedBikeStations.add(bikeStation4);
        allCachedBikeStations.add(bikeStation5);
        allCachedBikeStations.add(bikeStation6);
        allCachedBikeStations.add(bikeStation7);
        allCachedBikeStations.add(bikeStation8);
        allCachedBikeStations.add(bikeStation9);
        allCachedBikeStations.add(bikeStation10);

        testObserver = new TestObserver();
        searchBikeStations = new SearchBikeStations(
                new TestSchedulerProvider(),
                mockBikeStationsDataCache);

        when(mockBikeStationsDataCache.load()).thenReturn(Observable.just(allCachedBikeStations));
    }

    @Test
    public void searchBikeStations_matchedAtTheStart() {
        // execute use case with query string
        String query = "stat";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the correct search results in alphabetical order of featureName
        testObserver.assertValue(Arrays.asList(
                bikeStation1,
                bikeStation4,
                bikeStation5,
                bikeStation6,
                bikeStation2,
                bikeStation3,
                bikeStation7,
                bikeStation8)
        );
    }

    @Test
    public void searchBikeStations_matchedInTheMiddle() {
        // execute use case with query string
        String query = "1";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the correct search results in alphabetical order of featureName
        testObserver.assertValue(Arrays.asList(
                bikeStation1,
                bikeStation4,
                bikeStation5,
                bikeStation6)
        );
    }

    @Test
    public void searchBikeStations_matchedAtTheEnd() {
        // execute use case with query string
        String query = "kilda";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the correct search results in alphabetical order of featureName
        testObserver.assertValue(Arrays.asList(bikeStation10, bikeStation9));
    }

    @Test
    public void searchBikeStations_caseInsensitive() {
        // execute use case with query string
        String query = "aB";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the correct search results in alphabetical order of featureName
        testObserver.assertValue(Arrays.asList(bikeStation7, bikeStation8));
    }

    @Test
    public void searchBikeStations_ignoreWhiteSpaces() {
        // execute use case with query string
        String query = "  stationabc";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the correct search results in alphabetical order of featureName
        testObserver.assertValue(Collections.singletonList(bikeStation8));
    }

    @Test
    public void searchBikeStations_emptyQuery() {
        // execute use case with query string
        String query = "";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the correct search results in alphabetical order of featureName
        testObserver.assertValue(Collections.EMPTY_LIST);
    }

    @Test
    public void searchBikeStations_noMatches() {
        // execute use case with query string
        String query = "melbourne";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the correct search results in alphabetical order of featureName
        testObserver.assertValue(Collections.EMPTY_LIST);
    }

    @Test
    public void searchBikeStations_noCache() throws Exception {
        // given there's no cache data
        when(mockBikeStationsDataCache.load()).thenReturn(Observable.<List<BikeStation>>error(new CacheNotAvailableException()));

        // execute use case with query string
        String query = "station";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // verify that an exception is thrown and no result has been returned
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void searchBikeStations_cacheExpired() throws Exception {
        // given cache has expired
        when(mockBikeStationsDataCache.load()).thenReturn(Observable.<List<BikeStation>>error(new CacheExpiredException()));

        // execute use case with query string
        String query = "station";
        searchBikeStations.getStream(query).subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // verify that an exception is thrown and no result has been returned
        testObserver.assertError(CacheExpiredException.class);
        testObserver.assertNoValues();
    }
}
