package io.ychescale9.bikeshare.domain;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.bikeshare.TestSchedulerProvider;
import io.ychescale9.bikeshare.util.SchedulerProvider;

/**
 * Created by yang on 23/10/16.
 * Unit tests for the implementation of {@link UseCase}
 */
@SuppressWarnings("unchecked")
public class UseCaseTest {

    private final static String DUMMY_RESULT = "result";

    private TestObserver testObserver;

    private UseCaseImpl useCase;

    @Before
    public void setUp() {
        testObserver = new TestObserver();
        useCase = new UseCaseImpl(new TestSchedulerProvider());
    }

    @Test
    public void getStream() {
        useCase.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertValue(DUMMY_RESULT);
        testObserver.assertNoErrors();
    }

    private static class UseCaseImpl extends UseCase {

        UseCaseImpl(SchedulerProvider schedulerProvider) {
            super(schedulerProvider);
        }

        @Override
        protected Observable createUseCase() {
            return Observable.just(DUMMY_RESULT);
        }
    }
}
