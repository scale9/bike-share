package io.ychescale9.bikeshare.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.bikeshare.TestSchedulerProvider;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.data.repository.BikeStationRepository;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 23/10/16.
 * Unit tests for the implementation of {@link LoadAllBikeStations}
 */
@SuppressWarnings("unchecked")
public class LoadAllBikeStationsTest {

    private List<BikeStation> dummyBikeStations;

    @Mock
    private BikeStationRepository mockBikeStationRepository;

    private TestObserver testObserver;

    private LoadAllBikeStations loadAllBikeStations;

    @Before
    public void setUp() {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        dummyBikeStations = new ArrayList<>();
        dummyBikeStations.add(new BikeStation(1L, "station 1", "10000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(2L, "station 2", "20000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(3L, "station 3", "30000", 20, 0, null));

        testObserver = new TestObserver();
        loadAllBikeStations = new LoadAllBikeStations(
                new TestSchedulerProvider(),
                mockBikeStationRepository);
    }

    @Test
    public void loadBikeStations() {
        // given that loadAllBikeStations successfully returns a List<BikeStation> observable
        when(mockBikeStationRepository.loadAllBikeStations()).thenReturn(Observable.just(dummyBikeStations));

        // when the use case is executed
        loadAllBikeStations.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // should load all recordings
        verify(mockBikeStationRepository).loadAllBikeStations();

        // should return the correct list of bike stations
        testObserver.assertValue(dummyBikeStations);
    }
}
