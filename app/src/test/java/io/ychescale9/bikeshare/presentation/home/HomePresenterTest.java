package io.ychescale9.bikeshare.presentation.home;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;
import io.ychescale9.bikeshare.RxUtils;
import io.ychescale9.bikeshare.data.exception.CacheExpiredException;
import io.ychescale9.bikeshare.data.exception.CacheNotAvailableException;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.domain.LoadAllBikeStations;
import io.ychescale9.bikeshare.domain.SearchBikeStations;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 23/10/16.
 * Unit tests for the implementation of {@link HomePresenter}
 */
@SuppressWarnings("unchecked")
public class HomePresenterTest {

    private final int dataRefreshPollingIntervalMilliseconds = 300000; // 30 seconds

    private List<BikeStation> dummyBikeStations;

    private List<BikeStation> dummySearchResults;

    private BikeStation selectedBikeStation;

    @Mock
    private HomeContract.View mockView;

    @Mock
    private LoadAllBikeStations mockLoadAllBikeStations;

    @Mock
    private SearchBikeStations mockSearchBikeStations;

    private HomePresenter homePresenter;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        dummyBikeStations = new ArrayList<>();
        dummyBikeStations.add(new BikeStation(1L, "station 1", "10000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(2L, "station 2", "20000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(3L, "station 3", "30000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(10L, "station 10", "100000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(11L, "station 11", "110000", 20, 0, null));
        dummyBikeStations.add(new BikeStation(12L, "station 12", "120000", 20, 0, null));

        // dummy search results for "1"
        dummySearchResults = new ArrayList<>();
        dummySearchResults.add(dummyBikeStations.get(0));
        dummySearchResults.add(dummyBikeStations.get(3));
        dummySearchResults.add(dummyBikeStations.get(4));
        dummySearchResults.add(dummyBikeStations.get(5));

        selectedBikeStation = dummyBikeStations.get(2);

        homePresenter = new HomePresenter(
                mockView,
                mockLoadAllBikeStations,
                mockSearchBikeStations,
                dataRefreshPollingIntervalMilliseconds
        );

        // default stubs
        when(mockLoadAllBikeStations.getStream()).thenReturn(Observable.just(dummyBikeStations));
        when(mockSearchBikeStations.getStream(anyString())).thenReturn(Observable.just(dummySearchResults));
    }

    @Test
    public void loadAllBikeStations_success() throws Exception {
        // load all bike stations
        homePresenter.loadAllBikeStations();

        // should get use case stream
        verify(mockLoadAllBikeStations, times(2)).getStream();

        // should update UI with loaded bike stations
        verify(mockView).showAllBikeStations(dummyBikeStations);
    }

    @Test
    public void loadAllBikeStations_error() throws Exception {
        // given that mockLoadAllBikeStations.getStream() returns an error observable
        when(mockLoadAllBikeStations.getStream()).thenReturn(Observable.<List<BikeStation>>error(new Exception()));

        // load all bike stations
        homePresenter.loadAllBikeStations();

        // should get use case stream
        verify(mockLoadAllBikeStations, times(2)).getStream();

        // should update UI indicating an error has occurred while loading bike stations
        verify(mockView).showCannotLoadBikeStationsError();
    }

    @Test
    public void periodicallyLoadAllBikeStations() throws Exception {
        // Override schedulers with a test scheduler so we can manually advance clock
        TestScheduler testScheduler = new TestScheduler();
        RxUtils.overrideSchedulers(testScheduler);

        // load all bike stations to initiate periodic refresh
        homePresenter.loadAllBikeStations();

        reset(mockView);

        InOrder inOrder = inOrder(mockView);

        // should NOT update UI with loaded bike stations yet
        inOrder.verify(mockView, never()).showAllBikeStations(ArgumentMatchers.<BikeStation>anyList());

        // advance clock by dataRefreshPollingIntervalMilliseconds * 3
        testScheduler.advanceTimeBy(dataRefreshPollingIntervalMilliseconds * 3, TimeUnit.MILLISECONDS);

        // should have updated UI with loaded bike stations 3 times
        verify(mockView, times(3)).showAllBikeStations(dummyBikeStations);

        // reset schedulers after test
        RxUtils.resetSchedulers();
    }

    @Test
    public void periodicallyLoadAllBikeStations_error() throws Exception {
        // Override schedulers with a test scheduler so we can manually advance clock
        TestScheduler testScheduler = new TestScheduler();
        RxUtils.overrideSchedulers(testScheduler);

        reset(mockLoadAllBikeStations);

        // given that mockLoadAllBikeStations.getStream() returns an error observable initially
        when(mockLoadAllBikeStations.getStream()).thenReturn(Observable.<List<BikeStation>>error(new Exception()));

        // load all bike stations to initiate periodic refresh
        homePresenter.loadAllBikeStations();

        InOrder inOrder = inOrder(mockView);

        // should NOT update UI with loaded bike stations yet
        inOrder.verify(mockView, never()).showAllBikeStations(ArgumentMatchers.<BikeStation>anyList());

        reset(mockView);

        // advance clock by dataRefreshPollingIntervalMilliseconds
        testScheduler.advanceTimeBy(dataRefreshPollingIntervalMilliseconds, TimeUnit.MILLISECONDS);

        // should NOT update UI with loaded bike stations yet
        inOrder.verify(mockView, never()).showAllBikeStations(ArgumentMatchers.<BikeStation>anyList());

        // should update UI with error message
        inOrder.verify(mockView).showCannotLoadBikeStationsError();

        // given that mockLoadAllBikeStations.getStream() returns a dummy observable for the next interaction
        when(mockLoadAllBikeStations.getStream()).thenReturn(Observable.just(dummyBikeStations));

        // advance clock by another dataRefreshPollingIntervalMilliseconds
        testScheduler.advanceTimeBy(dataRefreshPollingIntervalMilliseconds, TimeUnit.MILLISECONDS);

        // should NOT have interacted with mockView at all as periodic refresh should have been terminated by the previous error
        inOrder.verifyNoMoreInteractions();

        // reset schedulers after test
        RxUtils.resetSchedulers();
    }

    @Test
    public void searchBikeStations() throws Exception {
        // load all bike stations
        homePresenter.loadAllBikeStations();

        // search bike stations with "1"
        homePresenter.searchBikeStations("1");

        // should get use case stream
        verify(mockSearchBikeStations).getStream(eq("1"));

        // should update UI with search results
        verify(mockView).showSearchResults(dummySearchResults);
    }

    @Test
    public void searchBikeStations_noMatch() throws Exception {
        // given that mockSearchBikeStations.getStream(anyString()) returns an empty observable
        when(mockSearchBikeStations.getStream(anyString())).thenReturn(Observable.just(Collections.<BikeStation>emptyList()));

        // load all bike stations
        homePresenter.loadAllBikeStations();

        // search bike stations with "100"
        homePresenter.searchBikeStations("100");

        // should get use case stream
        verify(mockSearchBikeStations).getStream(eq("100"));

        // should update UI to indicate no search results have been returned
        verify(mockView).showEmptySearchResultMessage();
    }

    @Test
    public void searchBikeStations_emptyQuery() throws Exception {
        // load all bike stations
        homePresenter.loadAllBikeStations();

        // search bike stations with ""
        homePresenter.searchBikeStations("");

        // should NOT get stream
        verify(mockSearchBikeStations, never()).getStream(anyString());

        // should NOT show search results
        verify(mockView, never()).showSearchResults(ArgumentMatchers.<BikeStation>anyList());

        // should NOT show no search results prompt message
        verify(mockView, never()).showEmptySearchResultMessage();

        // should hide search results
        verify(mockView).hideSearchResults();
    }

    @Test
    public void searchBikeStations_noCache() throws Exception {
        // given that mockSearchBikeStations.getStream(anyString()) returns an error observable with CacheNotAvailableException
        when(mockSearchBikeStations.getStream(anyString())).thenReturn(Observable.<List<BikeStation>>error(new CacheNotAvailableException()));

        // search bike stations with "station"
        homePresenter.searchBikeStations("station");

        // should reload all bike stations
        verify(mockLoadAllBikeStations, times(2)).getStream();
    }

    @Test
    public void searchBikeStations_cacheExpired() throws Exception {
        // given that mockSearchBikeStations.getStream(anyString()) returns an error observable with CacheExpiredException
        when(mockSearchBikeStations.getStream(anyString())).thenReturn(Observable.<List<BikeStation>>error(new CacheExpiredException()));

        // search bike stations with "station"
        homePresenter.searchBikeStations("station");

        // should reload all bike stations
        verify(mockLoadAllBikeStations, times(2)).getStream();
    }

    @Test
    public void selectBikeStation() throws Exception {
        // load all bike stations
        homePresenter.loadAllBikeStations();

        // user selects a bike station
        homePresenter.selectBikeStation(selectedBikeStation);

        // should update UI to show the selected bike station
        verify(mockView).showSelectedBikeStation(selectedBikeStation);

        // should exit search mode
        verify(mockView).exitSearchMode();

        // should have the correct currently selected station
        assertThat(homePresenter.getSelectedBikeStation().equals(selectedBikeStation), is(true));
    }

    @Test
    public void deselectBikeStation() throws Exception {
        // load all bike stations
        homePresenter.loadAllBikeStations();

        // user selects a bike station
        homePresenter.selectBikeStation(selectedBikeStation);

        // user then deselects the bike station
        homePresenter.deselectBikeStation();

        // should have NO selected station
        assertThat(homePresenter.getSelectedBikeStation(), is(nullValue()));
    }

    @Test
    public void getSelectedBikeStation() throws Exception {
        // load all bike stations
        homePresenter.loadAllBikeStations();

        // should have NO selected station
        assertThat(homePresenter.getSelectedBikeStation(), is(nullValue()));

        // user selects a bike station
        homePresenter.selectBikeStation(selectedBikeStation);

        // should have the correct currently selected station
        assertThat(homePresenter.getSelectedBikeStation().equals(selectedBikeStation), is(true));
    }

    @Test
    public void selectedBikeStationHasBeenUpdated() throws Exception {
        // load all bike stations
        homePresenter.loadAllBikeStations();

        // user selects a bike station
        homePresenter.selectBikeStation(selectedBikeStation);

        // at some point in the future all bike stations data is reloaded, with the selected bike station's value changed
        BikeStation updatedBikeStation = new BikeStation(3L, "station 3", "30000", 18, 2, null);
        dummyBikeStations.set(2, updatedBikeStation);
        homePresenter.loadAllBikeStations();

        // should have the correct currently selected station
        assertThat(homePresenter.getSelectedBikeStation().equals(updatedBikeStation), is(true));
    }

    @Test
    public void destroy() throws Exception {
        homePresenter.destroy();
    }

}