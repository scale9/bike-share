package io.ychescale9.bikeshare.presentation.util;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static io.ychescale9.bikeshare.presentation.util.MarkerSizeCalculator.ERROR_MESSAGE_BIKES_AVAILABLE_GREATER_THAN_MAX_BIKES_AVAILABLE;
import static io.ychescale9.bikeshare.presentation.util.MarkerSizeCalculator.ERROR_MESSAGE_BIKES_AVAILABLE_OR_MAX_BIKES_AVAILABLE_LESS_THAN_ZERO;
import static io.ychescale9.bikeshare.presentation.util.MarkerSizeCalculator.ERROR_MESSAGE_MIN_MARKER_SIZE_GREATER_THAN_MAX_MARKER_SIZE;
import static io.ychescale9.bikeshare.presentation.util.MarkerSizeCalculator.ERROR_MESSAGE_MIN_MARKER_SIZE_OR_MAX_MARKER_SIZE_NOT_GREATER_THAN_ZERO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by yang on 22/10/16.
 * Unit tests for the implementation of {@link MarkerSizeCalculator}
 */
public class MarkerSizeCalculatorTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {

    }

    @Test
    public void calculateMarkerSizeInPixel_smallest() throws Exception {
        int minSize = 10;
        int maxSize = 20;
        int bikesAvailable = 0;
        int maxBikesAvailable = 100;

        assertThat(
                MarkerSizeCalculator.calculateMarkerSizeInPixel(
                        minSize, maxSize, bikesAvailable, maxBikesAvailable),
                is(10));
    }

    @Test
    public void calculateMarkerSizeInPixel_largest() throws Exception {
        int minSize = 10;
        int maxSize = 20;
        int bikesAvailable = 100;
        int maxBikesAvailable = 100;

        assertThat(
                MarkerSizeCalculator.calculateMarkerSizeInPixel(
                        minSize, maxSize, bikesAvailable, maxBikesAvailable),
                is(20));
    }

    @Test
    public void calculateMarkerSizeInPixel_50Percent() throws Exception {
        int minSize = 10;
        int maxSize = 20;
        int bikesAvailable = 50;
        int maxBikesAvailable = 100;

        assertThat(
                MarkerSizeCalculator.calculateMarkerSizeInPixel(
                        minSize, maxSize, bikesAvailable, maxBikesAvailable),
                is(15));
    }

    @Test
    public void calculateMarkerSizeInPixel_invalidMinSizeOrMaxSize() throws Exception {
        thrown.expectMessage(ERROR_MESSAGE_MIN_MARKER_SIZE_OR_MAX_MARKER_SIZE_NOT_GREATER_THAN_ZERO);
        MarkerSizeCalculator.calculateMarkerSizeInPixel(-1, 5, 20, 50);

        thrown.expectMessage(ERROR_MESSAGE_MIN_MARKER_SIZE_OR_MAX_MARKER_SIZE_NOT_GREATER_THAN_ZERO);
        MarkerSizeCalculator.calculateMarkerSizeInPixel(0, 5, 20, 50);

        thrown.expectMessage(ERROR_MESSAGE_MIN_MARKER_SIZE_OR_MAX_MARKER_SIZE_NOT_GREATER_THAN_ZERO);
        MarkerSizeCalculator.calculateMarkerSizeInPixel(5, -1, 20, 50);

        thrown.expectMessage(ERROR_MESSAGE_MIN_MARKER_SIZE_OR_MAX_MARKER_SIZE_NOT_GREATER_THAN_ZERO);
        MarkerSizeCalculator.calculateMarkerSizeInPixel(5, 0, 20, 50);

        thrown.expectMessage(ERROR_MESSAGE_MIN_MARKER_SIZE_GREATER_THAN_MAX_MARKER_SIZE);
        MarkerSizeCalculator.calculateMarkerSizeInPixel(5, 3, 20, 50);
    }

    @Test
    public void calculateMarkerSizeInPixel_invalidBikesAvailableOrMaxBikesAvailable() throws Exception {
        thrown.expectMessage(ERROR_MESSAGE_BIKES_AVAILABLE_OR_MAX_BIKES_AVAILABLE_LESS_THAN_ZERO);
        MarkerSizeCalculator.calculateMarkerSizeInPixel(5, 10, -1, 50);

        thrown.expectMessage(ERROR_MESSAGE_BIKES_AVAILABLE_OR_MAX_BIKES_AVAILABLE_LESS_THAN_ZERO);
        MarkerSizeCalculator.calculateMarkerSizeInPixel(5, 10, 0, -1);

        thrown.expectMessage(ERROR_MESSAGE_BIKES_AVAILABLE_GREATER_THAN_MAX_BIKES_AVAILABLE);
        MarkerSizeCalculator.calculateMarkerSizeInPixel(5, 10, 20, 19);
    }
}
