package io.ychescale9.bikeshare.presentation.helper;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by yang on 22/10/16.
 * Model for holding map marker info including latLng and styling info (color and size)
 */
public class MarkerInfo {

    // station id uniquely identifies a marker on the map
    public Long stationId;

    // title of the marker
    public String title;

    // geolocation of the marker
    public LatLng latLng;

    // color of the marker
    public int color;

    // size of the marker in pixel
    public int size;

    public MarkerInfo(Long stationId, String title, LatLng latLng, int color, int size) {
        this.stationId = stationId;
        this.title = title;
        this.latLng = latLng;
        this.color = color;
        this.size = size;
    }
}
