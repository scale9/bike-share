package io.ychescale9.bikeshare.presentation.home.behavior;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import io.ychescale9.bikeshare.R;

/**
 * Created by yang on 1/11/16.
 * Behavior which moves the station info view as the {@link Snackbar} appears / disappears such that
 * the station info view is always at the top of any {@link Snackbar}.
 */
public class StationInfoViewBehavior extends CoordinatorLayout.Behavior<ViewGroup> {

    public StationInfoViewBehavior() {
    }

    public StationInfoViewBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ViewGroup child, View dependency) {
        return child == parent.findViewById(R.id.station_info_view) && dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ViewGroup child, View dependency) {
        float translationY = getStationInfoViewTranslationYForSnackbar(parent, child);
        child.setTranslationY(translationY);
        return false;
    }

    private float getStationInfoViewTranslationYForSnackbar(CoordinatorLayout parent, ViewGroup stationInfoView) {
        float minOffset = 0;
        final List<View> dependencies = parent.getDependencies(stationInfoView);
        for (int i = 0, z = dependencies.size(); i < z; i++) {
            final View view = dependencies.get(i);
            if (view instanceof Snackbar.SnackbarLayout && parent.doViewsOverlap(stationInfoView, view)) {
                minOffset = Math.min(minOffset,
                        ViewCompat.getTranslationY(view) - view.getHeight());
            }
        }
        return minOffset;
    }
}


