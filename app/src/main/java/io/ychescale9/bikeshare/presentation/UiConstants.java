package io.ychescale9.bikeshare.presentation;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by yang on 20/10/16.
 * Provides methods for getting app-level constant values
 */
public class UiConstants {

    private final static LatLng MELBOURNE_LAT_LNG = new LatLng(-37.827699, 144.957880);

    private final static float MAP_MIN_ZOOM_LEVEL = 10.0f;

    private final static float MAP_MAX_ZOOM_LEVEL = 14.0f;

    private final static float MAP_DEFAULT_ZOOM_LEVEL = 12.0f;

    public LatLng getMelbourneLatLng() {
        return MELBOURNE_LAT_LNG;
    }

    public float getMapMinZoomLevel() {
        return MAP_MIN_ZOOM_LEVEL;
    }

    public float getMapMaxZoomLevel() {
        return MAP_MAX_ZOOM_LEVEL;
    }

    public float getMapDefaultZoomLevel() {
        return MAP_DEFAULT_ZOOM_LEVEL;
    }
}
