package io.ychescale9.bikeshare.presentation.home;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.ychescale9.bikeshare.data.exception.CacheExpiredException;
import io.ychescale9.bikeshare.data.exception.CacheNotAvailableException;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.domain.LoadAllBikeStations;
import io.ychescale9.bikeshare.domain.SearchBikeStations;
import io.ychescale9.bikeshare.util.DefaultObserver;
import timber.log.Timber;

/**
 * Created by yang on 23/10/16.
 */
public class HomePresenter implements HomeContract.Presenter {

    private static final String TAG = HomePresenter.class.getSimpleName();

    private HomeContract.View mView;

    @NonNull
    private final LoadAllBikeStations mLoadAllBikeStations;

    @NonNull
    private final SearchBikeStations mSearchBikeStations;

    private final int mDataRefreshPollingIntervalMilliseconds;

    @NonNull
    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private Disposable mPeriodicDataRefreshDisposable;

    private BikeStation mSelectedBikeStation = null;

    HomePresenter(@NonNull HomeContract.View view,
                  @NonNull LoadAllBikeStations loadAllBikeStations,
                  @NonNull SearchBikeStations searchBikeStations,
                  int dataRefreshPollingIntervalMilliseconds) {
        Timber.tag(TAG);
        mView = view;
        mLoadAllBikeStations = loadAllBikeStations;
        mSearchBikeStations = searchBikeStations;
        mDataRefreshPollingIntervalMilliseconds = dataRefreshPollingIntervalMilliseconds;
    }

    @Override
    public void loadAllBikeStations() {
        mCompositeDisposable.add(
            mLoadAllBikeStations.getStream()
                .subscribeWith(new LoadAllBikeStationsObserver())
        );

        // start periodic data refresh
        startPeriodicDataRefresh();
    }

    @Override
    public void searchBikeStations(@NonNull String queryString) {
        if (!queryString.isEmpty()) {
            mCompositeDisposable.add(
                    mSearchBikeStations.getStream(queryString)
                            .subscribeWith(new SearchBikeStationsObserver())
            );
        } else {
            // hide search results if is query string is empty
            mView.hideSearchResults();
        }
    }

    @Override
    public void selectBikeStation(@NonNull BikeStation bikeStation) {
        Timber.d("Selected bike station: %s", bikeStation.toString());
        mSelectedBikeStation = bikeStation;
        mView.showSelectedBikeStation(mSelectedBikeStation);
        mView.exitSearchMode();
    }

    @Override
    public void deselectBikeStation() {
        Timber.d("Deselected bike station");
        mSelectedBikeStation = null;
    }

    @Override
    public BikeStation getSelectedBikeStation() {
        return mSelectedBikeStation;
    }

    @Override
    public void setView(@Nullable HomeContract.View view) {
        mView = view;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        mCompositeDisposable.clear();
        stopPeriodicDataRefresh();
        mView = null;
    }

    /**
     * Start periodically loading all bike stations data
     */
    @SuppressWarnings("unchecked")
    private void startPeriodicDataRefresh() {
        stopPeriodicDataRefresh();
        mPeriodicDataRefreshDisposable = mLoadAllBikeStations.getStream()
                .repeatWhen(new Function<Observable<Object>, ObservableSource<?>>() {
                    @Override
                    public ObservableSource<?> apply(Observable<Object> objectObservable) throws Exception {
                        return objectObservable.delay(mDataRefreshPollingIntervalMilliseconds, TimeUnit.MILLISECONDS);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .delaySubscription(mDataRefreshPollingIntervalMilliseconds, TimeUnit.MILLISECONDS)
                .subscribeWith(new LoadAllBikeStationsObserver());
    }

    private void stopPeriodicDataRefresh() {
        if (mPeriodicDataRefreshDisposable != null && !mPeriodicDataRefreshDisposable.isDisposed()) {
            mPeriodicDataRefreshDisposable.dispose();
            mPeriodicDataRefreshDisposable = null;
        }
    }

    /**
     * Find the bike station from the bikeStations list by id
     * @param bikeStations
     * @param id
     * @return
     */
    private BikeStation findBikeStationById(@NonNull List<BikeStation> bikeStations, Long id) {
        for (BikeStation bikeStation : bikeStations) {
            if (bikeStation.id.longValue() == id) {
                return bikeStation;
            }
        }
        return null;
    }

    private final class LoadAllBikeStationsObserver extends DefaultObserver<List<BikeStation>> {

        @Override
        public void onNext(List<BikeStation> allBikeStations) {
            if (!allBikeStations.isEmpty()) {
                // if a bike station is selected, update it if its values have changed
                if (mSelectedBikeStation != null) {
                    BikeStation bikeStation = findBikeStationById(allBikeStations, mSelectedBikeStation.id);
                    if (bikeStation != null && !bikeStation.equals(mSelectedBikeStation)) {
                        mSelectedBikeStation = bikeStation;
                        Timber.d("Selected bike station data has changed: %s", bikeStation);
                    }
                }
                // update UI with all bike stations data
                mView.showAllBikeStations(allBikeStations);
            } else {
                Timber.wtf("Loaded bike stations list is empty.");
            }
        }

        @Override
        public void onError(Throwable e) {
            Timber.w(e, "Error loading all bike stations.");
            mView.showCannotLoadBikeStationsError();

            // stop periodic data refresh
            stopPeriodicDataRefresh();
        }
    }

    private final class SearchBikeStationsObserver extends DefaultObserver<List<BikeStation>> {

        @Override
        public void onNext(List<BikeStation> searchResults) {
            if (searchResults.isEmpty()) {
                Timber.d("No bike stations found.");
                mView.showEmptySearchResultMessage();
            } else {
                mView.showSearchResults(searchResults);
            }
        }

        @Override
        public void onError(Throwable e) {
            if (e instanceof CacheNotAvailableException || e instanceof CacheExpiredException) {
                Timber.d(e, "No search results returned.");
                Timber.d("Reload all bike stations data immediately and restart periodic refresh.");
                loadAllBikeStations();
            } else {
                Timber.wtf(e);
            }
        }
    }
}
