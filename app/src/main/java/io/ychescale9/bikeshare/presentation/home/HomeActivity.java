package io.ychescale9.bikeshare.presentation.home;

import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.LongSparseArray;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import io.ychescale9.bikeshare.R;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.data.util.ModelUtil;
import io.ychescale9.bikeshare.presentation.UiConstants;
import io.ychescale9.bikeshare.presentation.base.BaseActivity;
import io.ychescale9.bikeshare.presentation.helper.MarkerInfo;
import io.ychescale9.bikeshare.presentation.util.MapUtils;
import io.ychescale9.bikeshare.presentation.util.MarkerSizeCalculator;
import io.ychescale9.bikeshare.presentation.util.UIUtils;
import io.ychescale9.bikeshare.util.GoogleApiUtil;
import timber.log.Timber;

public class HomeActivity extends BaseActivity<HomeContract.Presenter> implements HomeContract.View {

    private static final String TAG = HomeActivity.class.getSimpleName();

    private static final String EXTRA_IN_SEARCH_MODE = "IN_SEARCH_MODE";

    private GoogleMap mMap;

    // list of markers with info including styling info such as size and color
    private LongSparseArray<Pair<Marker, MarkerInfo>> mMarkersWithInfo = new LongSparseArray<>();

    @Inject
    UiConstants uiConstants;

    @Inject
    HomeContract.Presenter presenter;

    @BindView(R.id.edit_text_search_bike_stations)
    EditText searchBikeStationsEditText;

    @BindView(R.id.image_view_search)
    ImageView searchImageView;

    @BindView(R.id.button_cancel_search)
    ImageButton cancelButton;

    @BindView(R.id.button_clear)
    ImageButton clearButton;

    @BindView(R.id.card_view_no_results_prompt)
    CardView noResultsPromptCardView;

    @BindView(R.id.card_view_search_results)
    CardView searchResultsCardView;

    @BindView(R.id.recycler_view_search_results)
    RecyclerView searchResultsRecyclerView;

    @BindView(R.id.station_info_view)
    ViewGroup stationInfoView;

    @BindView(R.id.text_view_station_name)
    TextView stationNameTextView;

    @BindView(R.id.text_view_station_status)
    TextView stationStatusTextView;

    @BindView(R.id.shadow)
    View shadow;

    @BindView(R.id.fab_get_directions)
    FloatingActionButton getDirectionsButton;

    private SearchResultsAdapter mSearchResultsAdapter;

    private boolean mInSearchMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.tag(TAG);

        setContentView(R.layout.activity_home);

        if (!GoogleApiUtil.checkGoogleApiAvailability(this)) {
            Timber.w("Google Play Services not installed. Closing application now.");
            return;
        }

        ButterKnife.bind(this);

        mSearchResultsAdapter = new SearchResultsAdapter(this, searchResultsActionListener);
        searchResultsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchResultsRecyclerView.setAdapter(mSearchResultsAdapter);

        // add MapFragment to the container
        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = MapFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.map_container, mapFragment).commit();

        // get notified when the map is ready to be used.
        mapFragment.getMapAsync(onMapReadyCallback);

        // restore the activity state (whether in search mode)
        if (savedInstanceState != null) {
            mInSearchMode = savedInstanceState.getBoolean(EXTRA_IN_SEARCH_MODE, false);
            if (mInSearchMode) {
                enterSearchMode();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMap = null;
        mMarkersWithInfo.clear();
        mSearchResultsAdapter = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // save whether the UI is currently in search mode
        outState.putBoolean(EXTRA_IN_SEARCH_MODE, isInSearchMode());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (isInSearchMode()) {
            // if currently searching, exit search model
            exitSearchMode();
        }
        else if(isStationSelected()) {
            // if a station selected, clear the selection
            searchBikeStationsEditText.getText().clear();
            deselectBikeStation();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public HomeContract.Presenter getPresenter() {
        return presenter;
    }

    GoogleMap.OnMarkerClickListener onMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            Timber.d("onMarkerClick: %s", marker.getTitle());
            // do not show default info window as we are implementing our own
            return true;
        }
    };

    /**
     * Manipulate map with initial configurations.
     */
    OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            Timber.d("Google Map ready.");
            mMap = googleMap;
            mMap.setOnMarkerClickListener(onMarkerClickListener);

            // Set a preference for minimum and maximum zoom.
            mMap.setMinZoomPreference(uiConstants.getMapMinZoomLevel());
            mMap.setMaxZoomPreference(uiConstants.getMapMaxZoomLevel());

            // Move the camera to Melbourne
            LatLng melbourne = uiConstants.getMelbourneLatLng();
            mMap.moveCamera(CameraUpdateFactory.newLatLng(melbourne));
            // Set default zoom level
            mMap.moveCamera(CameraUpdateFactory.zoomTo(uiConstants.getMapDefaultZoomLevel()));

            // load all bike stations which will be plotted on the map
            presenter.loadAllBikeStations();
        }
    };

    @OnFocusChange(R.id.edit_text_search_bike_stations)
    public void searchBikeStationsEditTextOnClick(boolean hasFocus) {
        if (hasFocus) {
            enterSearchMode();
            // submit query immediately if edit text is NOT empty
            if (!isSearchBikeStationsEditTextEmpty()) {
                presenter.searchBikeStations(searchBikeStationsEditText.getText().toString());
            }
        } else {
            // make sure soft keyboard is closed
            UIUtils.hideKeyboard(this, searchBikeStationsEditText);
        }
    }

    @OnTextChanged(R.id.edit_text_search_bike_stations)
    public void searchBikeStationsEditTextOnTextChanged(Editable editable) {
        // start searching for bike stations if we are in search mode
        if (isInSearchMode()) {
            presenter.searchBikeStations(editable.toString());
        }

        if (isSearchBikeStationsEditTextEmpty()) {
            clearButton.setVisibility(View.GONE);
        } else {
            clearButton.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.button_cancel_search)
    public void cancelSearchButtonOnClick() {
        exitSearchMode();
    }

    @OnClick(R.id.button_clear)
    public void clearButtonOnClick() {
        // clear edit text
        searchBikeStationsEditText.getText().clear();
        if (!isInSearchMode()) {
            // we are not in search mode and are canceling the station selection
            deselectBikeStation();
        }
    }

    @OnClick(R.id.fab_get_directions)
    public void getDirectionsButtonOnClick() {
        // launch Google Maps turn-by-turn navigation to the address of the selected station
        if (isStationSelected()) {
            BikeStation selectedBikeStation = presenter.getSelectedBikeStation();
            Uri uri = MapUtils.buildNavigationUri(selectedBikeStation.coordinates);
            MapUtils.startMapsIntent(this, uri);
        }
    }

    /**
     * Action listener for the search results adapter
     */
    SearchResultsAdapter.ActionListener searchResultsActionListener = new SearchResultsAdapter.ActionListener() {
        @Override
        public void onItemClick(@NonNull BikeStation bikeStation) {
            // populate the edit text with the full bike station feature name
            searchBikeStationsEditText.setText(bikeStation.featureName);
            // if a marker (station) is already selected, update the marker to deselected state first
            if (isStationSelected()) {
                updateMarker(presenter.getSelectedBikeStation(), false);
            }
            // select bike station
            presenter.selectBikeStation(bikeStation);
        }
    };

    /**
     * Button click listener for retrying to load bike stations
     */
    View.OnClickListener onRetryButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            presenter.loadAllBikeStations();
        }
    };

    @Override
    public void showAllBikeStations(@NonNull List<BikeStation> bikeStations) {
        // enable search edit text
        searchBikeStationsEditText.setEnabled(true);

        if (mMap != null) {
            // clear any markers on the map
            mMap.clear();
            // clear markers info
            mMarkersWithInfo.clear();

            // add markers for all bike stations
            for (BikeStation bikeStation : bikeStations) {
                String markerTitle = bikeStation.featureName;
                int color = ContextCompat.getColor(this, R.color.marker);
                int markerSizePx;
                try {
                    markerSizePx = MarkerSizeCalculator.calculateMarkerSizeInPixel(
                            getResources().getDimensionPixelSize(R.dimen.min_marker_size),
                            getResources().getDimensionPixelSize(R.dimen.max_marker_size),
                            bikeStation.bikesAvailable,
                            ModelUtil.getMaxBikesAvailable(bikeStations)
                    );
                } catch (Exception e) {
                    Timber.e(e, "Could not generate marker due to invalid size, skipping this station.");
                    continue;
                }

                Marker newMarker = mMap.addMarker(new MarkerOptions()
                        .position(bikeStation.coordinates)
                        .flat(true)
                        .icon(BitmapDescriptorFactory.fromBitmap(getScaledMarkerIcon(markerSizePx, false)))
                        .title(markerTitle));

                mMarkersWithInfo.put(bikeStation.id, new Pair<>(newMarker,
                                new MarkerInfo(
                                        bikeStation.id,
                                        bikeStation.featureName,
                                        bikeStation.coordinates,
                                        color,
                                        markerSizePx
                                )));
            }
        }

        // resubmit query if we are (were) in search mode e.g. restore state after orientation change
        if (isInSearchMode()) {
            presenter.searchBikeStations(searchBikeStationsEditText.getText().toString());
        }

        // re-select bike station marker in the UI if we have (had) a selected station e.g. restore state after orientation change
        if (isStationSelected()) {
            selectBikeStation(presenter.getSelectedBikeStation());
        }
    }

    @Override
    public void showCannotLoadBikeStationsError() {
        // disable search edit text
        searchBikeStationsEditText.setEnabled(false);

        // show error message in a snackbar with a retry button
        Snackbar.make(
                findViewById(R.id.root_view),
                getString(R.string.error_message_could_not_load_bike_stations), Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.button_text_retry, onRetryButtonClickListener)
                .show();
    }


    @Override
    public void enterSearchMode() {
        mInSearchMode = true;
        searchImageView.setVisibility(View.GONE);
        cancelButton.setVisibility(View.VISIBLE);
        toggleStationInfoView(false); // hide this even if a station is selected
    }

    @Override
    public void exitSearchMode() {
        mInSearchMode = false;
        UIUtils.hideKeyboard(this, searchBikeStationsEditText);
        searchBikeStationsEditText.clearFocus();

        searchImageView.setVisibility(View.VISIBLE);
        cancelButton.setVisibility(View.GONE);

        hideSearchResults();

        if (!isStationSelected()) {
            // clear edit text if no bike station is currently selected
            searchBikeStationsEditText.getText().clear();
        } else {
            // otherwise repopulate the edit text with the full bike station feature name
            searchBikeStationsEditText.setText(presenter.getSelectedBikeStation().featureName);
            // and show the station info view
            toggleStationInfoView(true);
        }
    }

    @Override
    public void showSearchResults(@NonNull List<BikeStation> bikeStations) {
        // only show results if we are still in search mode
        if (isInSearchMode()) {
            if (mSearchResultsAdapter != null && !bikeStations.isEmpty()) {
                searchResultsRecyclerView.scrollToPosition(0);
                mSearchResultsAdapter.setBikeStations(bikeStations);
                mSearchResultsAdapter.notifyDataSetChanged();
            }

            noResultsPromptCardView.setVisibility(View.GONE);
            searchResultsCardView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showEmptySearchResultMessage() {
        // only show no result message if we are still in search mode
        if (isInSearchMode()) {
            noResultsPromptCardView.setVisibility(View.VISIBLE);
            searchResultsCardView.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideSearchResults() {
        noResultsPromptCardView.setVisibility(View.GONE);
        searchResultsCardView.setVisibility(View.GONE);
    }

    @Override
    public void showSelectedBikeStation(@NonNull BikeStation bikeStation) {
        // update UI to show the selected bike station
        selectBikeStation(bikeStation);
    }

    /**
     * Display UI elements representing the station selection being selected
     */
    private void selectBikeStation(@NonNull BikeStation bikeStation) {
        // update the marker to selected state
        updateMarker(bikeStation, true);

        // Move the camera to the selected marker
        mMap.moveCamera(CameraUpdateFactory.newLatLng(bikeStation.coordinates));

        // Set the zoom level to maximum
        mMap.animateCamera(CameraUpdateFactory.zoomTo(uiConstants.getMapMaxZoomLevel()));

        // populate station info view (with the selected bike station values)
        populateStationInfoView();

        // show the station info view only if we are NOT in search mode
        if (!isInSearchMode()) {
            toggleStationInfoView(true);
        }
    }

    /**
     * Clean up UI elements representing the bike station being deselected
     */
    private void deselectBikeStation() {
        // get the bike station to be deselected
        BikeStation bikeStationToDeselect = presenter.getSelectedBikeStation();

        // update the marker to deselected state
        updateMarker(bikeStationToDeselect, false);

        // Move the camera back to melbourne
        LatLng melbourne = uiConstants.getMelbourneLatLng();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(melbourne));

        // Set the zoom level back to default
        mMap.animateCamera(CameraUpdateFactory.zoomTo(uiConstants.getMapDefaultZoomLevel()));

        // hide the station info view
        toggleStationInfoView(false);

        // synchronize selection state in presenter
        presenter.deselectBikeStation();
    }

    /**
     * Update the marker on the map to a new state (selected/deselected)
     * @param bikeStation - the bike station which the marker being updated represents
     * @param selected - whether the marker should be updated to selected state
     */
    private void updateMarker(BikeStation bikeStation, boolean selected) {
        // find the marker being updated
        Pair<Marker, MarkerInfo> markerWithInfo = mMarkersWithInfo.get(bikeStation.id);
        if (markerWithInfo == null) {
            Timber.e("Cannot update marker for station '%s' as it's not rendered on the map.", bikeStation);
            return;
        }

        // remove the marker from the map as we replacing it with a new one
        markerWithInfo.first.remove();

        // update marker color
        MarkerInfo updatedMarkerInfo = markerWithInfo.second;
        updatedMarkerInfo.color = ContextCompat.getColor(this, selected ? R.color.marker_selected : R.color.marker);
        // create a new marker object with the new color
        Marker updatedMarker = mMap.addMarker(new MarkerOptions()
                .position(updatedMarkerInfo.latLng)
                .flat(true)
                .icon(BitmapDescriptorFactory.fromBitmap(getScaledMarkerIcon(updatedMarkerInfo.size, selected)))
                .title(updatedMarkerInfo.title));
        // replace the entry with a new one with the updated marker and marker info
        mMarkersWithInfo.put(bikeStation.id, new Pair<>(updatedMarker, updatedMarkerInfo));
    }

    /**
     * Populate the station info view with values of the selected station
     */
    private void populateStationInfoView() {
        if (isStationSelected()) {
            // populate station name
            BikeStation selectedBikeStation = presenter.getSelectedBikeStation();
            stationNameTextView.setText(selectedBikeStation.featureName);

            // construct the station status string
            String numberOfBikes;
            if (selectedBikeStation.bikesAvailable == 0) {
                numberOfBikes = getString(R.string.no_bikes_available);
            }
            else if (selectedBikeStation.bikesAvailable == 1) {
                numberOfBikes = getString(R.string.one_bike_available);
            } else {
                numberOfBikes = getString(R.string.number_of_bikes_available, selectedBikeStation.bikesAvailable);
            }
            String numberOfSlots;
            if (selectedBikeStation.emptySlotsAvailable == 0) {
                numberOfSlots = getString(R.string.no_slots_available);
            }
            else if (selectedBikeStation.emptySlotsAvailable == 1) {
                numberOfSlots = getString(R.string.one_slot_available);
            } else {
                numberOfSlots = getString(R.string.number_of_slots_available, selectedBikeStation.emptySlotsAvailable);
            }
            String stationStatusString = numberOfBikes + " \u2022 " + numberOfSlots;
            stationStatusTextView.setText(stationStatusString);
        }
    }

    /**
     * Show or hide the station info view at the bottom
     * @param on
     */
    private void toggleStationInfoView(boolean on) {
        if (on) {
            stationInfoView.setVisibility(View.VISIBLE);
            shadow.setVisibility(View.VISIBLE);
            getDirectionsButton.setVisibility(View.VISIBLE);
        } else {
            stationInfoView.setVisibility(View.GONE);
            shadow.setVisibility(View.GONE);
            getDirectionsButton.setVisibility(View.GONE);
        }
    }

    private Bitmap getScaledMarkerIcon(int markerSize, boolean selected) {
        // convert shape drawable to bitmap
        Drawable drawable = ContextCompat.getDrawable(this, selected ? R.drawable.circle_marker_selected : R.drawable.circle_marker);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        // scale bitmap to markerSize
        return Bitmap.createScaledBitmap(bitmap, markerSize, markerSize, false);
    }

    private boolean isSearchBikeStationsEditTextEmpty() {
        return searchBikeStationsEditText.getText().toString().isEmpty();
    }

    private boolean isInSearchMode() {
        return mInSearchMode;
    }

    private boolean isStationSelected() {
        return presenter.getSelectedBikeStation() != null;
    }

    @VisibleForTesting
    public LatLng getMapCameraPosition() {
        if (mMap != null) {
            return mMap.getCameraPosition().target;
        } else {
            return null;
        }
    }

    @VisibleForTesting
    public LongSparseArray<Pair<Marker, MarkerInfo>> getMarkersWithInfo() {
        return mMarkersWithInfo;
    }
}
