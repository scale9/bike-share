package io.ychescale9.bikeshare.presentation.base;

/**
 * Created by yang on 20/10/16.
 */
public interface IPresenter<T> {

    /**
     * Call this to update view instance associated with a presenter on orientation change.
     */
    void setView(T view);

    /**
     * Call this in the view's (Activity or Fragment) onPause() method.
     */
    void pause();

    /**
     * Call this in the view's (Activity or Fragment) onResume() method.
     */
    void resume();

    /**
     * Call this in the view's (Activity or Fragment) onDestroy() method.
     */
    void destroy();

}
