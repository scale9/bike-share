package io.ychescale9.bikeshare.presentation.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public final class UIUtils {
	
	public static void hideKeyboard(Activity activity, View focusedView) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    }
}
