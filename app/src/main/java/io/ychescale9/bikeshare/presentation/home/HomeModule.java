package io.ychescale9.bikeshare.presentation.home;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.bikeshare.data.api.ApiConstants;
import io.ychescale9.bikeshare.data.cache.BikeStationsDataCache;
import io.ychescale9.bikeshare.data.repository.BikeStationRepository;
import io.ychescale9.bikeshare.domain.LoadAllBikeStations;
import io.ychescale9.bikeshare.domain.SearchBikeStations;
import io.ychescale9.bikeshare.presentation.base.ViewScope;
import io.ychescale9.bikeshare.presentation.util.PresenterManager;
import io.ychescale9.bikeshare.util.SchedulerProvider;

/**
 * Created by yang on 20/10/16.
 */
@Module
public class HomeModule {

    @Provides
    @ViewScope
    HomeContract.View provideHomeView(HomeActivity activity) {
        return activity;
    }

    @Provides
    HomeContract.Presenter provideHomePresenter(HomeContract.View homeView,
                                                LoadAllBikeStations loadAllBikeStations,
                                                SearchBikeStations searchBikeStations,
                                                ApiConstants apiConstants) {
        HomeContract.Presenter presenter;
        presenter = (HomeContract.Presenter) PresenterManager.getInstance().restorePresenter(homeView.getClass());
        if (presenter == null) {
            presenter = new HomePresenter(
                    homeView,
                    loadAllBikeStations,
                    searchBikeStations,
                    apiConstants.getApiUpdateIntervalMilliseconds()
            );
        } else {
            presenter.setView(homeView);
        }
        return presenter;
    }

    @Provides
    LoadAllBikeStations provideLoadAllBikeStations(SchedulerProvider schedulerProvider, BikeStationRepository bikeStationRepository) {
        return new LoadAllBikeStations(
                schedulerProvider,
                bikeStationRepository
        );
    }

    @Provides
    SearchBikeStations provideSearchBikeStations(SchedulerProvider schedulerProvider, BikeStationsDataCache bikeStationsDataCache) {
        return new SearchBikeStations(
                schedulerProvider,
                bikeStationsDataCache
        );
    }
}
