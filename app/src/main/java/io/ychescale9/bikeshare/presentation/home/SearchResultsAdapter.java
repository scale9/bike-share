package io.ychescale9.bikeshare.presentation.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.ychescale9.bikeshare.R;
import io.ychescale9.bikeshare.data.model.BikeStation;

/**
 * Created by yang on 25/05/16.
 */
public class SearchResultsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface ActionListener {
        void onItemClick(@NonNull BikeStation bikeStation);
    }

    private final Context mContext;
    private final ActionListener mActionListener;

    private List<BikeStation> mBikeStations;

    public SearchResultsAdapter(Context context, ActionListener actionListener) {
        mContext = context;
        mBikeStations = new ArrayList<>();
        mActionListener = actionListener;
    }

    public void setBikeStations(@NonNull List<BikeStation> bikeStations){
        mBikeStations = bikeStations;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View rowView;
        rowView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_bike_stations_search_result, viewGroup, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder vh = (ViewHolder) viewHolder;
        final BikeStation bikeStation = mBikeStations.get(i);
        vh.mStationNameTextView.setText(bikeStation.featureName);
        String numberOfBikes;
        if (bikeStation.bikesAvailable == 0) {
            numberOfBikes = mContext.getString(R.string.no_bikes_available);
        }
        else if (bikeStation.bikesAvailable == 1) {
            numberOfBikes = mContext.getString(R.string.one_bike_available);
        } else {
            numberOfBikes = mContext.getString(R.string.number_of_bikes_available, bikeStation.bikesAvailable);
        }
        String numberOfSlots;
        if (bikeStation.emptySlotsAvailable == 0) {
            numberOfSlots = mContext.getString(R.string.no_slots_available);
        }
        else if (bikeStation.emptySlotsAvailable == 1) {
            numberOfSlots = mContext.getString(R.string.one_slot_available);
        } else {
            numberOfSlots = mContext.getString(R.string.number_of_slots_available, bikeStation.emptySlotsAvailable);
        }
        String stationStatusString = numberOfBikes + " \u2022 " + numberOfSlots;
        vh.mStationStatusTextView.setText(stationStatusString);
        vh.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActionListener != null) {
                    mActionListener.onItemClick(bikeStation);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBikeStations.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.root_view) LinearLayout mRootView;
        @BindView(R.id.text_view_station_name) TextView mStationNameTextView;
        @BindView(R.id.text_view_station_status) TextView mStationStatusTextView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
