package io.ychescale9.bikeshare.presentation.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import dagger.android.AndroidInjection;
import io.ychescale9.bikeshare.presentation.util.PresenterManager;

/**
 * Created by yang on 20/10/16.
 */
public abstract class BaseActivity<P extends IPresenter> extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            if (getPresenter() != null) {
                getPresenter().destroy();
                PresenterManager.getInstance().removePresenter(getClass());
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getPresenter() != null) {
            PresenterManager.getInstance().savePresenter(getClass(), getPresenter());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (getPresenter() != null) {
            getPresenter().pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getPresenter() != null) {
            getPresenter().resume();
        }
    }

    public abstract P getPresenter();
}
