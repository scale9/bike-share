package io.ychescale9.bikeshare.presentation.base;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import io.ychescale9.bikeshare.presentation.home.HomeActivity;
import io.ychescale9.bikeshare.presentation.home.HomeModule;

/**
 * Created by yang on 29/4/17.
 */
@Module
public abstract class ActivityModule {

    @ViewScope
    @ContributesAndroidInjector(modules = HomeModule.class)
    abstract HomeActivity homeActivity();
}
