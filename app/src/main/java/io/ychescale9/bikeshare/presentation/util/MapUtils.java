package io.ychescale9.bikeshare.presentation.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

public final class MapUtils {

    /**
     * Build a uri with the coordinates passed in for GoogleMaps navigation from the current location.
     * @param coordinates
     * @return
     */
	public static Uri buildNavigationUri(@NonNull LatLng coordinates) {
        String uriStringFormat = "https://maps.google.com/maps?f=d&daddr=%s,%s";
        String uriString = String.format(uriStringFormat, coordinates.latitude, coordinates.longitude);
        return Uri.parse(uriString);
    }

    /**
     * Start a Google Maps intent with the passed in uri.
     * @param context
     * @param uri
     */
    public static void startMapsIntent(@NonNull Context context, @NonNull Uri uri) {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
        mapIntent.setPackage("com.google.android.apps.maps");
        context.startActivity(mapIntent);
    }
}
