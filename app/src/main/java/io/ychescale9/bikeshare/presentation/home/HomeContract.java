package io.ychescale9.bikeshare.presentation.home;

import android.support.annotation.NonNull;

import java.util.List;

import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.presentation.base.IPresenter;

/**
 * Created by yang on 23/10/16.
 */
public interface HomeContract {

    interface View {

        void showAllBikeStations(@NonNull List<BikeStation> bikeStations);

        void showCannotLoadBikeStationsError();

        void enterSearchMode();

        void exitSearchMode();

        void showSearchResults(@NonNull List<BikeStation> bikeStations);

        void showEmptySearchResultMessage();

        void hideSearchResults();

        void showSelectedBikeStation(@NonNull BikeStation bikeStation);
    }

    interface Presenter extends IPresenter<View> {

        void loadAllBikeStations();

        void searchBikeStations(@NonNull String queryString);

        void selectBikeStation(@NonNull BikeStation bikeStation);

        void deselectBikeStation();

        BikeStation getSelectedBikeStation();
    }
}
