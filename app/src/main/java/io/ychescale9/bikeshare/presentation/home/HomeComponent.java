package io.ychescale9.bikeshare.presentation.home;

import dagger.Subcomponent;
import io.ychescale9.bikeshare.presentation.base.ViewScope;

/**
 * Created by yang on 20/10/16.
 */
@ViewScope
@Subcomponent(
        modules = HomeModule.class
)
public interface HomeComponent {

    void inject(HomeActivity activity);
}
