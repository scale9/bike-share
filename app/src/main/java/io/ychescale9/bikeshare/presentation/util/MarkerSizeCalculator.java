package io.ychescale9.bikeshare.presentation.util;

/**
 * Created by yang on 22/10/16.
 */
public class MarkerSizeCalculator {

    public final static String ERROR_MESSAGE_MIN_MARKER_SIZE_OR_MAX_MARKER_SIZE_NOT_GREATER_THAN_ZERO = "minMarkerSizePx and maxMarkerSizePx marker size must be greater than zero.";
    public final static String ERROR_MESSAGE_MIN_MARKER_SIZE_GREATER_THAN_MAX_MARKER_SIZE = "minMarkerSizePx must NOT be greater than maxMarkerSizePx.";
    public final static String ERROR_MESSAGE_BIKES_AVAILABLE_OR_MAX_BIKES_AVAILABLE_LESS_THAN_ZERO = "bikesAvailable and maxBikesAvailable must NOT be less than zero.";
    public final static String ERROR_MESSAGE_BIKES_AVAILABLE_GREATER_THAN_MAX_BIKES_AVAILABLE = "bikesAvailable must NOT be greater than maxBikesAvailable.";

    /**
     * Calculate the size of the marker representing the bike station based on the number of bikes available
     * and the maximum number of bikes available among a list of bike stations.
     * The calculated size will be within the range of the min and max size in pixel.
     *
     * @param minMarkerSizePx - minimum marker size in pixel
     * @param maxMarkerSizePx - maximum marker size in pixel
     * @param bikesAvailable - number of bikes available at the station which the marker represents
     * @param maxBikesAvailable - maximum number of bikes available among a list of stations
     * @return
     */
    public static int calculateMarkerSizeInPixel(int minMarkerSizePx, int maxMarkerSizePx, int bikesAvailable, int maxBikesAvailable) throws Exception {
        // min size and max size must be greater than than 0
        if (minMarkerSizePx <= 0 || maxMarkerSizePx <= 0) {
            throw new Exception(ERROR_MESSAGE_MIN_MARKER_SIZE_OR_MAX_MARKER_SIZE_NOT_GREATER_THAN_ZERO);
        }
        // min size must not be greater than max size
        if (minMarkerSizePx > maxMarkerSizePx) {
            throw new Exception(ERROR_MESSAGE_MIN_MARKER_SIZE_GREATER_THAN_MAX_MARKER_SIZE);
        }
        // bikesAvailable and maxBikesAvailable must be greater than than 0
        if (bikesAvailable < 0 || maxBikesAvailable < 0) {
            throw new Exception(ERROR_MESSAGE_BIKES_AVAILABLE_OR_MAX_BIKES_AVAILABLE_LESS_THAN_ZERO);
        }
        // bikesAvailable must not be greater than maxBikesAvailable
        if (bikesAvailable > maxBikesAvailable) {
            throw new Exception(ERROR_MESSAGE_BIKES_AVAILABLE_GREATER_THAN_MAX_BIKES_AVAILABLE);
        }

        float ratio = (float) bikesAvailable / maxBikesAvailable;
        return Math.round(minMarkerSizePx + (maxMarkerSizePx - minMarkerSizePx) * ratio);
    }
}
