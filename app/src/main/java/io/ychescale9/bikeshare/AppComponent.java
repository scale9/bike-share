package io.ychescale9.bikeshare;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import io.ychescale9.bikeshare.data.api.ApiModule;
import io.ychescale9.bikeshare.data.cache.CacheModule;
import io.ychescale9.bikeshare.data.repository.RepositoryModule;
import io.ychescale9.bikeshare.presentation.base.ActivityModule;

/**
 * Created by yang on 20/10/16.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                ConstantsModule.class,
                RepositoryModule.class,
                ApiModule.class,
                CacheModule.class,
                ActivityModule.class, // all activity sub-components
        }
)
interface AppComponent {

    void inject(BaseBikeShareApplication application);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(Application application);

        AppComponent build();
    }
}
