package io.ychescale9.bikeshare.data.util;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import io.ychescale9.bikeshare.data.model.BikeStation;
import timber.log.Timber;

/**
 * Created by yang on 20/10/16.
 */
public class BikeStationJsonDeserializer implements JsonDeserializer<BikeStation> {

    private static final String TAG = BikeStationJsonDeserializer.class.getSimpleName();

    public BikeStationJsonDeserializer() {
        Timber.tag(TAG);
    }

    @Override
    public BikeStation deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        // parse id
        Long id = jsonObject.get("id").getAsLong();

        // parse featureName
        String featureName = jsonObject.get("featurename").getAsString();

        // parse terminalName
        String terminalName = jsonObject.get("terminalname").getAsString();

        // parse bikesAvailable
        int bikesAvailable = jsonObject.get("nbbikes").getAsInt();

        // parse emptySlotsAvailable
        int emptySlotsAvailable = jsonObject.get("nbemptydoc").getAsInt();

        // parse coordinates
        JsonArray coordinates = jsonObject.getAsJsonObject("coordinates").getAsJsonArray("coordinates");
        // NOTE: the coordinates in the json data has longitude first and latitude second
        // which is opposite to the order of the parameters for LatLng
        double longitude = coordinates.get(0).getAsDouble();
        double latitude = coordinates.get(1).getAsDouble();
        LatLng latLng = new LatLng(latitude, longitude);

        return new BikeStation(id, featureName, terminalName, bikesAvailable, emptySlotsAvailable, latLng);
    }
}
