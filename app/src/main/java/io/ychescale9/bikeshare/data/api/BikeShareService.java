package io.ychescale9.bikeshare.data.api;

import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.bikeshare.data.model.BikeStation;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by yang on 23/10/16.
 */

public interface BikeShareService {

    @GET
    Observable<List<BikeStation>> loadAllBikeStations(@Url String url);
}
