package io.ychescale9.bikeshare.data.cache;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.ychescale9.bikeshare.data.exception.CacheExpiredException;
import io.ychescale9.bikeshare.data.exception.CacheNotAvailableException;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.util.Clock;
import timber.log.Timber;

/**
 * Created by yang on 20/10/16.
 * Cache layer of the list of all bike stations
 */
public class BikeStationsDataCacheImpl implements BikeStationsDataCache {

    private static final String TAG = BikeStationsDataCacheImpl.class.getSimpleName();

    private final int mExpirationTimeMilliseconds;

    private final Clock mClock;

    private final List<BikeStation> mCachedBikeStations = new ArrayList<>();

    private long mLastUpdateTime = 0;

    public BikeStationsDataCacheImpl(int expirationTimeMilliseconds, @NonNull Clock clock) {
        Timber.tag(TAG);
        mExpirationTimeMilliseconds = expirationTimeMilliseconds;
        mClock = clock;
    }

    @Override
    public Observable<List<BikeStation>> load() {
        return Observable.fromCallable(new Callable<List<BikeStation>>() {
            @Override
            public List<BikeStation> call() throws Exception {
                if (mCachedBikeStations.isEmpty()) {
                    Timber.d("Bike stations cache is empty, not returning cache data");
                    throw new CacheNotAvailableException();
                }
                if (isExpired()) {
                    Timber.d("Bike stations cache has expired, not returning cache data");
                    clear();
                    throw new CacheExpiredException();
                }
                return new ArrayList<>(mCachedBikeStations);
            }
        });
    }

    @Override
    public void update(@NonNull List<BikeStation> bikeStations) {
        // only update cache if new data is NOT empty
        if (!bikeStations.isEmpty()) {
            Timber.d("Updating bike stations cache.");
            // replace current bikeStations with new bikeStations
            mCachedBikeStations.clear();
            mCachedBikeStations.addAll(bikeStations);
            // set last update time
            mLastUpdateTime = System.currentTimeMillis();
        }
    }

    @Override
    public void clear() {
        Timber.d("Clearing cache.");
        mCachedBikeStations.clear();
        mLastUpdateTime = 0;
    }

    @Override
    public boolean isExpired() {
        return (mClock.getCurrentTimeMillis() - mLastUpdateTime) > mExpirationTimeMilliseconds;
    }
}
