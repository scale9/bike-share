package io.ychescale9.bikeshare.data.util;

import android.support.annotation.NonNull;

import java.util.List;

import io.ychescale9.bikeshare.data.model.BikeStation;

/**
 * Created by yang on 22/10/16.
 */
public class ModelUtil {

    public final static String ERROR_MESSAGE_BIKE_STATIONS_IS_EMPTY = "bike stations must NOT be empty.";

    /**
     * Get the maximum number of bikes available among a list of bike stations
     * @param bikeStations
     * @return
     */
    public static int getMaxBikesAvailable(@NonNull List<BikeStation> bikeStations) throws Exception {
        if (bikeStations.size() == 0) {
            throw new Exception(ERROR_MESSAGE_BIKE_STATIONS_IS_EMPTY);
        }
        int result = 0;
        for (BikeStation bikeStation : bikeStations) {
            if (result < bikeStation.bikesAvailable) {
                result = bikeStation.bikesAvailable;
            }
        }
        return result;
    }
}
