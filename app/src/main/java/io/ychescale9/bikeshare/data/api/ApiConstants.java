package io.ychescale9.bikeshare.data.api;

import io.ychescale9.bikeshare.BuildConfig;

/**
 * Created by yang on 22/10/16.
 * Provides methods for getting app-level constant values
 */
public class ApiConstants {

    public String getApiEndpoint() {
        return BuildConfig.API_ENDPOINT;
    }

    public int getConnectTimeOutSeconds() {
        return BuildConfig.API_CONNECT_TIMEOUT_SECONDS;
    }

    public int getReadTimeOutSeconds() {
        return BuildConfig.API_READ_TIMEOUT_SECONDS;
    }

    public int getWriteTimeOutSeconds() {
        return BuildConfig.API_WRITE_TIMEOUT_SECONDS;
    }

    public int getApiUpdateIntervalMilliseconds() {
        return BuildConfig.API_UPDATE_FREQUENCY_MILLISECONDS;
    }
}
