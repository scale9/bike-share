package io.ychescale9.bikeshare.data.cache;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.bikeshare.data.api.ApiConstants;
import io.ychescale9.bikeshare.util.Clock;

/**
 * Created by yang on 19/06/2016.
 */
@Module
public class CacheModule {

    @Provides
    @Singleton
    BikeStationsDataCache provideBikeStationsDataCache(ApiConstants apiConstants, Clock clock) {
        return new BikeStationsDataCacheImpl(apiConstants.getApiUpdateIntervalMilliseconds(), clock);
    }
}
