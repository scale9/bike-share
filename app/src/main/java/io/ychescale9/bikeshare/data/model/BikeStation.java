package io.ychescale9.bikeshare.data.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by yang on 22/10/16.
 */
public class BikeStation {

    public Long id;

    public String featureName;

    public String terminalName;

    public int bikesAvailable;

    public int emptySlotsAvailable;

    public LatLng coordinates;

    public BikeStation(Long id, String featureName, String terminalName, int bikesAvailable, int emptySlotsAvailable, LatLng coordinates) {
        this.id = id;
        this.featureName = featureName;
        this.terminalName = terminalName;
        this.bikesAvailable = bikesAvailable;
        this.emptySlotsAvailable = emptySlotsAvailable;
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return featureName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BikeStation that = (BikeStation) o;

        if (bikesAvailable != that.bikesAvailable) return false;
        if (emptySlotsAvailable != that.emptySlotsAvailable) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (featureName != null ? !featureName.equals(that.featureName) : that.featureName != null)
            return false;
        if (terminalName != null ? !terminalName.equals(that.terminalName) : that.terminalName != null)
            return false;
        return coordinates != null ? coordinates.equals(that.coordinates) : that.coordinates == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (featureName != null ? featureName.hashCode() : 0);
        result = 31 * result + (terminalName != null ? terminalName.hashCode() : 0);
        result = 31 * result + bikesAvailable;
        result = 31 * result + emptySlotsAvailable;
        result = 31 * result + (coordinates != null ? coordinates.hashCode() : 0);
        return result;
    }
}
