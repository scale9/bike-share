package io.ychescale9.bikeshare.data.exception;

/**
 * Created by yang on 23/10/16.
 * Exception for when the {@link io.ychescale9.bikeshare.data.cache.BikeStationsDataCache} is empty.
 */
public class CacheNotAvailableException extends Exception {

    public CacheNotAvailableException() {
        super();
    }

    public CacheNotAvailableException(final String message) {
        super(message);
    }

    public CacheNotAvailableException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CacheNotAvailableException(final Throwable cause) {
        super(cause);
    }
}
