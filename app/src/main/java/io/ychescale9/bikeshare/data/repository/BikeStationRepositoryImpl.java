package io.ychescale9.bikeshare.data.repository;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.ychescale9.bikeshare.data.api.BikeShareService;
import io.ychescale9.bikeshare.data.cache.BikeStationsDataCache;
import io.ychescale9.bikeshare.data.model.BikeStation;
import timber.log.Timber;

/**
 * Created by yang on 20/10/16.
 */
public class BikeStationRepositoryImpl implements BikeStationRepository {

    private static final String TAG = BikeStationRepositoryImpl.class.getSimpleName();

    private final BikeStationsDataCache mBikeStationsDataCache;
    private final BikeShareService mBikeShareService;
    private final String mServiceUrl;

    public BikeStationRepositoryImpl(@NonNull BikeStationsDataCache bikeStationsDataCache,
                                     @NonNull BikeShareService bikeShareService,
                                     @NonNull String serviceUrl) {
        Timber.tag(TAG);
        mBikeStationsDataCache = bikeStationsDataCache;
        mBikeShareService = bikeShareService;
        mServiceUrl = serviceUrl;
    }

    @Override
    public Observable<List<BikeStation>> loadAllBikeStations() {
        // try to load bike stations from cache first,
        // if cache does not exist or has expired load data from service
        Observable<List<BikeStation>> loadDataFromService = mBikeShareService.loadAllBikeStations(mServiceUrl)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        Timber.d("Loading bike stations data from server.");
                    }
                })
                .doOnNext(new Consumer<List<BikeStation>>() {
                    @Override
                    public void accept(List<BikeStation> bikeStations) throws Exception {
                        Timber.d("Data loaded from server.");

                        // update cache once bike stations data has been loaded from service
                        if (!bikeStations.isEmpty()) {
                            Timber.d("Updating cache with loaded bikeStations from service");
                            mBikeStationsDataCache.update(bikeStations);
                        }
                    }
                });

        return mBikeStationsDataCache.load()
                .onExceptionResumeNext(loadDataFromService)
                .takeWhile(new Predicate<List<BikeStation>>() {
                    @Override
                    public boolean test(List<BikeStation> bikeStations) throws Exception {
                        return !bikeStations.isEmpty();
                    }
                });
    }
}
