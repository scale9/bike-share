package io.ychescale9.bikeshare.data.repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.bikeshare.data.api.ApiConstants;
import io.ychescale9.bikeshare.data.api.BikeShareService;
import io.ychescale9.bikeshare.data.cache.BikeStationsDataCache;

/**
 * Created by yang on 20/10/16.
 */
@Module
public class RepositoryModule {

    @Provides
    @Singleton
    BikeStationRepository provideBikeStationRepository(BikeStationsDataCache bikeStationsDataCache,
                                                       BikeShareService bikeShareService,
                                                       ApiConstants apiConstants) {
        return new BikeStationRepositoryImpl(bikeStationsDataCache, bikeShareService, apiConstants.getApiEndpoint());
    }
}
