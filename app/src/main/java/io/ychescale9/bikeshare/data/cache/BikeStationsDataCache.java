package io.ychescale9.bikeshare.data.cache;

import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.bikeshare.data.model.BikeStation;

/**
 * Created by yang on 20/10/16.
 */
public interface BikeStationsDataCache {

    Observable<List<BikeStation>> load();

    void update(List<BikeStation> bikeStations);

    void clear();

    boolean isExpired();
}
