package io.ychescale9.bikeshare.data.repository;

import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.bikeshare.data.model.BikeStation;

/**
 * Created by yang on 20/10/16.
 */
public interface BikeStationRepository {

    Observable<List<BikeStation>> loadAllBikeStations();
}
