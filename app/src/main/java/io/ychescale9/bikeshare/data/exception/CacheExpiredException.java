package io.ychescale9.bikeshare.data.exception;

/**
 * Created by yang on 23/10/16.
 * Exception for when the {@link io.ychescale9.bikeshare.data.cache.BikeStationsDataCache} has expired.
 */
public class CacheExpiredException extends Exception {

    public CacheExpiredException() {
        super();
    }

    public CacheExpiredException(final String message) {
        super(message);
    }

    public CacheExpiredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CacheExpiredException(final Throwable cause) {
        super(cause);
    }
}
