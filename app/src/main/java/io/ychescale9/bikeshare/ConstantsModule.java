package io.ychescale9.bikeshare;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.bikeshare.data.api.ApiConstants;
import io.ychescale9.bikeshare.presentation.UiConstants;

/**
 * Created by yang on 20/10/16.
 */
@Module
public class ConstantsModule {

    @Provides
    @Singleton
    UiConstants provideUiConstants() {
        return new UiConstants();
    }

    @Provides
    @Singleton
    ApiConstants provideApiConstants() {
        return new ApiConstants();
    }
}