package io.ychescale9.bikeshare;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.bikeshare.data.util.BikeStationJsonDeserializer;
import io.ychescale9.bikeshare.util.Clock;
import io.ychescale9.bikeshare.util.RealClock;
import io.ychescale9.bikeshare.util.SchedulerProvider;

/**
 * Created by yang on 20/10/16.
 */
@Module
public class AppModule {

    @Provides
    @Singleton
    Clock provideClock() {
        return new RealClock();
    }

    @Provides
    @Singleton
    BikeStationJsonDeserializer provideBikeStationJsonDeserializer() {
        return new BikeStationJsonDeserializer();
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }
}