package io.ychescale9.bikeshare.util;

/**
 * Created by yang on 24/10/16.
 */
public class RealClock implements Clock {
    @Override
    public long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }
}
