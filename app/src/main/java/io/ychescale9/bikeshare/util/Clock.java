package io.ychescale9.bikeshare.util;

/**
 * Created by yang on 24/10/16.
 */
public interface Clock {
    long getCurrentTimeMillis();
}
