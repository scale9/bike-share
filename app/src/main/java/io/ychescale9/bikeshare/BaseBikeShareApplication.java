package io.ychescale9.bikeshare;

import android.app.Activity;
import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by yang on 20/10/16.
 */
public abstract class BaseBikeShareApplication extends Application implements HasActivityInjector {

    private AppComponent component;

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        // create and inject app component
        component = createComponent();
        component.inject(this);

        // initialize leak detection
        LeakCanary.install(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }

    /**
     * Create top-level Dagger app component.
     */
    protected AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .app(this)
                .build();
    }

    public AppComponent getComponent() {
        return component;
    }
}
