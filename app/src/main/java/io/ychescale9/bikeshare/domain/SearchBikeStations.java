package io.ychescale9.bikeshare.domain;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.ychescale9.bikeshare.data.cache.BikeStationsDataCache;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.util.SchedulerProvider;
import timber.log.Timber;

/**
 * Created by yang on 23/10/16.
 */
public class SearchBikeStations extends UseCase<List<BikeStation>> {

    private static final String TAG = SearchBikeStations.class.getSimpleName();

    private final BikeStationsDataCache mBikeStationsDataCache;
    private String mQueryString;

    public SearchBikeStations(SchedulerProvider schedulerProvider,
                              BikeStationsDataCache mockBikeStationsDataCache) {
        super(schedulerProvider);
        Timber.tag(TAG);
        mBikeStationsDataCache = mockBikeStationsDataCache;
    }

    public Observable<List<BikeStation>> getStream(String queryString) {
        this.mQueryString = queryString;
        return super.getStream();
    }

    @Override
    protected Observable<List<BikeStation>> createUseCase() {
        // remove white spaces in query string and convert to lower case
        final String cleanQueryString = mQueryString.replaceAll("\\s", "").toLowerCase();

        // return an empty list of results if search query is empty
        if (cleanQueryString.isEmpty()) {
            Timber.d("Search query is empty, returning empty result.");
            return Observable.just(Collections.<BikeStation>emptyList());
        }

        return mBikeStationsDataCache.load()
                .flatMap(new Function<List<BikeStation>, Observable<List<BikeStation>>>() {
                    @Override
                    public Observable<List<BikeStation>> apply(List<BikeStation> bikeStations) throws Exception {
                        return Observable.fromIterable(bikeStations)
                                .filter(new Predicate<BikeStation>() {
                                    @Override
                                    public boolean test(BikeStation bikeStation) throws Exception {
                                        String cleanFeatureName = bikeStation.featureName.replaceAll("\\s", "").toLowerCase();
                                        // remove white spaces in feature name and convert to lower case
                                        return cleanFeatureName.contains(cleanQueryString);
                                    }
                                })
                                .toSortedList(new Comparator<BikeStation>() {
                                    @Override
                                    public int compare(BikeStation bikeStation, BikeStation bikeStation2) {
                                        // sort the results returned in alphabetical order of featureName
                                        return bikeStation.featureName.compareToIgnoreCase(bikeStation2.featureName);
                                    }
                                })
                                .toObservable();
                    }
                });
    }
}
