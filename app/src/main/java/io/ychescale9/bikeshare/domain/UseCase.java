package io.ychescale9.bikeshare.domain;

import io.reactivex.Observable;
import io.ychescale9.bikeshare.util.SchedulerProvider;

/**
 * Created by yang on 20/10/16.
 */
public abstract class UseCase<T> {

    private final SchedulerProvider schedulerProvider;

    UseCase(SchedulerProvider schedulerProvider) {
        this.schedulerProvider = schedulerProvider;
    }

    protected abstract Observable<T> createUseCase();

    public Observable<T> getStream() {
        return createUseCase().compose(schedulerProvider.<T>applySchedulers());
    }
}