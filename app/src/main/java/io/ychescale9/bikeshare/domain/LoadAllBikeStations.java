package io.ychescale9.bikeshare.domain;

import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.data.repository.BikeStationRepository;
import io.ychescale9.bikeshare.util.SchedulerProvider;

/**
 * Created by yang on 23/10/16.
 */
public class LoadAllBikeStations extends UseCase<List<BikeStation>> {

    private final BikeStationRepository mBikeStationRepository;

    public LoadAllBikeStations(SchedulerProvider schedulerProvider,
                               BikeStationRepository bikeStationRepository) {
        super(schedulerProvider);
        mBikeStationRepository = bikeStationRepository;
    }

    @Override protected Observable<List<BikeStation>> createUseCase() {
        return mBikeStationRepository.loadAllBikeStations();
    }
}
