package io.ychescale9.bikeshare;

import timber.log.Timber;

/**
 * Created by yang on 9/11/16.
 */
public class BikeShareApplication extends BaseBikeShareApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        // install the release tree
        Timber.plant(new ReleaseTree());
    }
}
