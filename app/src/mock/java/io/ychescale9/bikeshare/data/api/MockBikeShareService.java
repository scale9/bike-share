package io.ychescale9.bikeshare.data.api;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.bikeshare.data.MockData;
import io.ychescale9.bikeshare.data.model.BikeStation;
import io.ychescale9.bikeshare.data.util.BikeStationJsonDeserializer;
import retrofit2.mock.BehaviorDelegate;

/**
 * Created by yang on 23/10/16.
 */
public class MockBikeShareService implements BikeShareService {

    private final BehaviorDelegate<BikeShareService> mDelegate;
    private final BikeStationJsonDeserializer mBikeStationJsonDeserializer;

    public MockBikeShareService(@NonNull BehaviorDelegate<BikeShareService> delegate,
                                @NonNull BikeStationJsonDeserializer bikeStationJsonDeserializer) {
        mDelegate = delegate;
        mBikeStationJsonDeserializer = bikeStationJsonDeserializer;
    }

    @Override
    public Observable<List<BikeStation>> loadAllBikeStations(String url) {
        // create Gson object with custom deserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(BikeStation.class, mBikeStationJsonDeserializer);
        Gson gson = gsonBuilder.create();
        // convert the mock data from json to List<BikeStation>
        Type listType = new TypeToken<List<BikeStation>>() {}.getType();
        List<BikeStation> bikeStations = gson.fromJson(MockData.BIKE_STATIONS_JSON, listType);

        return mDelegate.returningResponse(bikeStations).loadAllBikeStations(url);
    }
}
