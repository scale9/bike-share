package io.ychescale9.bikeshare.data.api;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.bikeshare.data.util.BikeStationJsonDeserializer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

/**
 * Created by yang on 20/10/16.
 */
@Module
public class ApiModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit(ApiConstants apiConstants) {
        return new Retrofit.Builder()
                .baseUrl(apiConstants.getApiEndpoint())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    BikeShareService provideBikeShareService(Retrofit retrofit, BikeStationJsonDeserializer deserializer) {
        NetworkBehavior behavior = NetworkBehavior.create();
        MockRetrofit mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build();
        BehaviorDelegate<BikeShareService> delegate = mockRetrofit.create(BikeShareService.class);

        return new MockBikeShareService(delegate, deserializer);
    }
}
